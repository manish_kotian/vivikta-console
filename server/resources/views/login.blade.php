<!DOCTYPE html>
<head>
    @include('header');
    <title>Login</title>
</head>
<body class="login-content" ng-app="viviktaConsole" ng-controller="LoginController">
    <div class="lc-block toggled" id="l-login">
        <img src="img/vivikta_logo.png" style="width: 70px;height: 70px;">
        <h3>Vivikta Console</h3>
        <p>Login to continue</p>
        <div class="input-group m-b-20">
            <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
            <div class="fg-line">
                <input type="emailId" class="form-control" placeholder="Username" ng-model="loginData.emailId">
            </div>
        </div>

        <div class="input-group m-b-20">
            <span class="input-group-addon"><i class="zmdi zmdi-lock"></i></span>
            <div class="fg-line">
                <input type="password" class="form-control" placeholder="Password" ng-model="loginData.password">
            </div>
        </div>
        <div class="clearfix"></div>
        <a href="" class="btn btn-login btn-danger btn-float" ng-click="doLogin(loginData)"><i class="zmdi zmdi-arrow-forward"></i></a>
    </div>
    @include('scripts');
</body>
</html>