<!DOCTYPE html>
<html>
<head>
    <title>Vivikta Console | Deployment Settings</title>
    @include('header');
</head>
<body ng-app="viviktaConsole" class="ng-cloak" ng-controller="DeploymentsController" ng-init="loadDeploymentSettings()">
<header id="header" class="ng-cloak">
    <ul class="header-inner ng-cloak">
        <li id="menu-trigger" data-trigger="#sidebar">
            <div class="line-wrap">
                <div class="line top"></div>
                <div class="line center"></div>
                <div class="line bottom"></div>
            </div>
        </li>
    </ul>
    <div id="top-search-wrap">
        <input type="text">
        <i id="top-search-close">&times;</i>
    </div>
</header>
<section id="main" class="ng-cloak">
    <aside id="sidebar" class="ng-cloak">
        @include('sidebar');
    </aside>
    <section id="content" class="ng-cloak">
        <div class="container ng-cloak">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card card-padding">
                        <div class="card-header bgm-bluegray">
                            <h2><span style="font-weight: bold;"><%deploymentDetails.deploymentName | uppercase%></span> Settings</h2>
                        </div>
                        <div class="card-body card-padding">
                            <div class="row">
                                <div class='col-sm-6'>
                                    Deployment Status
                                </div>
                                <div class='col-sm-6'>
                                    <li id="deploymentEnableStatus" style="list-style: none;">
                                        <div class="toggle-switch">
                                            <input id="tw-switch" type="checkbox" hidden="hidden">
                                            <label for="tw-switch" class="ts-helper"></label>
                                        </div>
                                    </li>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
<footer id="footer" ng-controller="TimerController" class="ng-cloak">
    @include('footer');
    <div id="timer">
    </div>
</footer>
@include('scripts');
</body>
</html>