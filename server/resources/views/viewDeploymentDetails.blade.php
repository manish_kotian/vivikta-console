<!DOCTYPE html>
<html>
<head>
    <title>Vivikta Console | Deployment Details</title>
    @include('header');
</head>
<body ng-app="viviktaConsole" class="ng-cloak" ng-controller="DeploymentsController" ng-init="loadDeploymentDetails()">
<header id="header" class="ng-cloak">
    <ul class="header-inner ng-cloak">
        <li id="menu-trigger" data-trigger="#sidebar">
            <div class="line-wrap">
                <div class="line top"></div>
                <div class="line center"></div>
                <div class="line bottom"></div>
            </div>
        </li>
    </ul>
    <div id="top-search-wrap">
        <input type="text">
        <i id="top-search-close">&times;</i>
    </div>
</header>
<section id="main" class="ng-cloak">
    <aside id="sidebar" class="ng-cloak">
        @include('sidebar');
    </aside>
    <section id="content" class="ng-cloak">
        <div class="container ng-cloak">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card card-padding">
                        <div class="card-header bgm-bluegray">
                            <h2><%deploymentDetails.deploymentName | uppercase%> Deployment Details</h2>
                        </div>
                        <div class="card-body card-padding">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Id:</label>
                                <span style="display: block;font-weight: bold;font-size: 15px;"><%deploymentDetails.deploymentId%></span>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Description:</label>
                                <span style="display: block;font-weight: bold;font-size: 15px;"><%deploymentDetails.deploymentDescription%></span>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Deployment Url:</label>
                                <span style="display: block;font-weight: bold;font-size: 15px;"><%deploymentDetails.deploymentUrl%></span>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Deployment Directory Path:</label>
                                <span style="display: block;font-weight: bold;font-size: 15px;"><%deploymentDetails.deploymentDirectoryPath%></span>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Deployment Virtual Host File Name:</label>
                                <span style="display: block;font-weight: bold;font-size: 15px;"><%deploymentDetails.deploymentVirtualHostName%></span>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Vendor Id:</label>
                                <span style="display: block;font-weight: bold;font-size: 15px;"><%deploymentDetails.deploymentVendorId%></span>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Vendor:</label>
                                <span style="display: block;font-weight: bold;font-size: 15px;"><%deploymentDetails.deploymentVendorName%></span>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Deployment Hosted On:</label>
                                <span style="display: block;font-weight: bold;font-size: 15px;"><%deploymentDetails.deploymentServerName%></span>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Deployment Project Id:</label>
                                <span style="display: block;font-weight: bold;font-size: 15px;"><%deploymentDetails.deploymentProjectId%></span>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Deployment Project Name:</label>
                                <span style="display: block;font-weight: bold;font-size: 15px;"><%deploymentDetails.deploymentProjectName%></span>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Deployment Status:</label>
                                <span style="display: block;font-weight: bold;font-size: 15px;">
                                            <span ng-if="deploymentDetails.deploymentStatus == 0">
                                                <i class="zmdi zmdi-circle" style="color: green;"></i>
                                                Enabled
                                            </span>
                                            <span ng-if="deploymentDetails.deploymentStatus == 1">
                                                <i class="zmdi zmdi-circle" style="color: red;"></i>
                                                Disabled
                                            </span>
                                        </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
<footer id="footer" ng-controller="TimerController" class="ng-cloak">
    @include('footer');
    <div id="timer">
    </div>
</footer>
@include('scripts');
</body>
</html>