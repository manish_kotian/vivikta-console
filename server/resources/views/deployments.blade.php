<!DOCTYPE html>
<html>
<head>
    <title>Vivikta Console | Deployments</title>
    @include('header');
</head>
<body ng-app="viviktaConsole" class="ng-cloak" ng-controller="DeploymentsController" ng-init="listAllDeployments(0,'')">
<header id="header" class="ng-cloak">
    <ul class="header-inner ng-cloak">
        <li id="menu-trigger" data-trigger="#sidebar">
            <div class="line-wrap">
                <div class="line top"></div>
                <div class="line center"></div>
                <div class="line bottom"></div>
            </div>
        </li>
        <li class="logo hidden-xs">
            <a href="{{ url('deployments') }}">Deployments</a>
        </li>
    </ul>
    <div id="top-search-wrap">
        <input type="text">
        <i id="top-search-close">&times;</i>
    </div>
</header>
<section id="main" class="ng-cloak">
    <aside id="sidebar" class="ng-cloak">
        @include('sidebar');
    </aside>
    <section id="content" class="ng-cloak">
        <div class="container ng-cloak">
            <div class="block-header">
                <h2>Deployments</h2>
                <ul class="actions" style="margin-right: 7%">
                    <li>
                        <a href="">
                            <button class="btn btn-info btn-icon-text waves-effect" ng-click="toggleDeploymentsForm()"><i class="fa fa-plus"></i> Add Deployment</button>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="card" id="listDeployments" ng-if="deploymentsFound">
                <div class="table-responsive" style="min-height: 300px;">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Url</th>
                            <th>Vendor</th>
                            <th>Server</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="deployment in deployments">
                            <td><%$index+1%></td>
                            <td>
                                <%deployment.deploymentName%>
                            </td>
                            <td>
                                <%deployment.deploymentUrl%>
                            </td>
                            <td>
                                <%deployment.deploymentVendorName%>
                            </td>
                            <td>
                                <%deployment.deploymentServerName%>
                            </td>
                            <td>
                                            <span ng-if="deployment.deploymentStatus == 0">
                                                <i class="zmdi zmdi-circle" style="color: green;"></i>
                                                Enabled
                                            </span>
                                <span ng-if="deployment.deploymentStatus == 1">
                                                <i class="zmdi zmdi-circle" style="color: red;"></i>
                                                Disabled
                                            </span>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
                                        Action
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a ng-click="viewDeploymentDetails(deployment)">View Details</a>
                                        </li>
                                        <li>
                                            <a ng-click="deploymentSettings(deployment)">Settings</a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card bgm-red" id="listDeployments" ng-if="!deploymentsFound">
                <div class="row ng-cloak text-center">
                    <div class="col-md-12 ng-cloak">
                        <h2 style="color: white;">No Deployments Found!</h2>
                    </div>
                </div>
            </div>
            <div class="card ng-cloak" id="addDeployments" style="display: none">
                <div class="card-header">
                    <h2>Configure Deployment</h2>
                </div>
                <div class="card-body card-padding">
                    <form name = "addDeploymentsForm">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Name:</label>
                            <input type="text" class="form-control" id="recipient-name1" ng-model="deploymentDetails.deploymentName" required>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Description:</label>
                            <input type="text" class="form-control" id="recipient-name1" ng-model="deploymentDetails.deploymentDescription" required>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Directory Path:</label>
                            <input type="text" class="form-control" id="recipient-name1" ng-model="deploymentDetails.deploymentDirectoryPath" required>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Deployment Url:</label>
                            <input type="text" class="form-control" id="recipient-name1" ng-model="deploymentDetails.deploymentUrl" required>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Virtual Host Name:</label>
                            <input type="text" class="form-control" id="recipient-name1" ng-model="deploymentDetails.deploymentVirtualHostName" required>
                        </div>
                        <div class="form-group" ng-init="getVendorsList()">
                            <label for="recipient-name" class="control-label">Vendor:</label>
                            <select class="form-control" ng-model="deploymentDetails.deploymentVendorId">
                                <option ng-repeat="vendor in vendors" value="<%vendor.vendorId%>"><%vendor.vendorName%></option>
                            </select>
                        </div>
                        <div class="form-group" ng-init="getServersList()">
                            <label for="recipient-name" class="control-label">Hosted On:</label>
                            <select class="form-control" ng-model="deploymentDetails.deploymentHostedOn">
                                <option ng-repeat="server in servers" value="<%server.serverId%>"><%server.serverName%></option>
                            </select>
                        </div>
                        <div class="form-group" ng-init="getProjectsList()">
                            <label for="recipient-name" class="control-label">Project:</label>
                            <select class="form-control" ng-model="deploymentDetails.deploymentProjectId">
                                <option ng-repeat="project in projects" value="<%project.appId%>"><%project.appName%></option>
                            </select>
                        </div>
                        <button class="btn btn-default waves-effect" ng-click="toggleDeploymentsForm()">Close</button>
                        <button class="btn btn-primary waves-effect" ng-click="addDeployment(deploymentDetails)" ng-disabled="addDeploymentsForm.$invalid">Add</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</section>
<footer id="footer" ng-controller="TimerController" class="ng-cloak">
    @include('footer');
    <div id="timer">
    </div>
</footer>
@include('scripts');
</body>
</html>