<!DOCTYPE html>
<html>
<head>
    <title>Vivikta Console | Project Details</title>
    @include('header');
</head>
<body ng-app="viviktaConsole" class="ng-cloak" ng-controller="ProjectController" ng-init="loadProjectDetailsByAppId()">
<header id="header" class="ng-cloak">
    <ul class="header-inner ng-cloak">
        <li id="menu-trigger" data-trigger="#sidebar">
            <div class="line-wrap">
                <div class="line top"></div>
                <div class="line center"></div>
                <div class="line bottom"></div>
            </div>
        </li>
    </ul>
    <div id="top-search-wrap">
        <input type="text">
        <i id="top-search-close">&times;</i>
    </div>
</header>
<section id="main" class="ng-cloak">
    <aside id="sidebar" class="ng-cloak">
        @include('sidebar');
    </aside>
    <section id="content" class="ng-cloak">
        <div class="container ng-cloak">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card card-padding">
                        <div class="card-header bgm-bluegray">
                            <h2><%projectDetails.appName | uppercase%></h2>
                        </div>
                        <div class="card-body card-padding">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Id:</label>
                                <span style="display: block;font-weight: bold;font-size: 15px;"><%projectDetails.appId%></span>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Name:</label>
                                <span style="display: block;font-weight: bold;font-size: 15px;"><%projectDetails.appName%></span>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Description:</label>
                                <span style="display: block;font-weight: bold;font-size: 15px;"><%projectDetails.appDescription%></span>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Database:</label>
                                <span style="display: block;font-weight: bold;font-size: 15px;"><%projectDetails.databaseName%></span>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Database Username:</label>
                                <span style="display: block;font-weight: bold;font-size: 15px;"><%projectDetails.databaseUsername%></span>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Database Password:</label>
                                <span style="display: block;font-weight: bold;font-size: 15px;"><%projectDetails.databasePassword%></span>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Gitlab Link:</label>
                                <span style="display: block;font-weight: bold;font-size: 15px;"><%projectDetails.gitlabLink%></span>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Gitlab Username:</label>
                                <span style="display: block;font-weight: bold;font-size: 15px;"><%projectDetails.gitlabUsername%></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
<footer id="footer" ng-controller="TimerController" class="ng-cloak">
    @include('footer');
    <div id="timer">
    </div>
</footer>
@include('scripts');
</body>
</html>