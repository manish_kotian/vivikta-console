<!DOCTYPE html>
<head>
    <title>Vivikta Console | Dashboard</title>
    @include('header');
</head>
<body ng-app="viviktaConsole" class="ng-cloak" ng-controller="DashboardController" ng-init="loadDashboardData()">
<header id="header" class="ng-cloak">
    <ul class="header-inner ng-cloak">
        <li id="menu-trigger" data-trigger="#sidebar">
            <div class="line-wrap">
                <div class="line top"></div>
                <div class="line center"></div>
                <div class="line bottom"></div>
            </div>
        </li>
        <li class="logo hidden-xs">
            <a href="{{ url('dashboard') }}">Dashboard</a>
        </li>
    </ul>
    <div id="top-search-wrap">
        <input type="text">
        <i id="top-search-close">&times;</i>
    </div>
</header>
<section id="main" class="ng-cloak">
    <aside id="sidebar" class="ng-cloak">
        @include('sidebar');
    </aside>
    <section id="content" class="ng-cloak">
        <div class="container ng-cloak">
            <div class="block-header ng-cloak">
                <h2>Dashboard</h2>
                <ul class="actions">
                    <li class="dropdown">
                        <a href="" data-toggle="dropdown">
                            <i class="zmdi zmdi-more-vert"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a href="">Refresh</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="dash-widgets ng-cloak">
                <div class="row">
                    <a href="{{ url('clients') }}">
                        <div class="col-md-3 col-sm-6">
                            <div id="pie-charts" class="dash-widget-item" style="min-height: 0px;">
                                <div class="bgm-lightgreen">
                                    <div class="text-center p-20 m-t-25">
                                        <div class="easy-pie main-pie">
                                            <div class="percent percent-without-symbol"><%totalVendorsCount%></div>
                                            <div class="pie-title">Vendors Count</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="{{ url('servers') }}">
                        <div class="col-md-3 col-sm-6">
                            <div id="pie-charts" class="dash-widget-item" style="min-height: 0px;">
                                <div class="bgm-bluegray">
                                    <div class="text-center p-20 m-t-25">
                                        <div class="easy-pie main-pie">
                                            <div class="percent percent-without-symbol"><%totalServersCount%></div>
                                            <div class="pie-title">Servers Count</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="{{ url('projects') }}">
                        <div class="col-md-3 col-sm-6">
                            <div id="pie-charts" class="dash-widget-item" style="min-height: 0px;">
                                <div class="bgm-pink">
                                    <div class="text-center p-20 m-t-25">
                                        <div class="easy-pie main-pie">
                                            <div class="percent percent-without-symbol"><%totalAppCount%></div>
                                            <div class="pie-title">Application Count</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="{{ url('deployments') }}">
                        <div class="col-md-3 col-sm-6">
                            <div id="pie-charts" class="dash-widget-item" style="min-height: 0px;">
                                <div class="bgm-amber">
                                    <div class="text-center p-20 m-t-25">
                                        <div class="easy-pie main-pie">
                                            <div class="percent percent-without-symbol"><%totalDeploymentsCount%></div>
                                            <div class="pie-title">Deployments Count</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
</section>

<footer id="footer" ng-controller="TimerController" class="ng-cloak">
    @include('footer');
    <div id="timer">
    </div>
</footer>
    @include('scripts');
</body>
</html>