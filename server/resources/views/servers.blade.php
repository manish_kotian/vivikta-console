<!DOCTYPE html>
<html>
<head>
    <title>Vivikta Console | Servers</title>
    @include('header');
</head>
<body ng-app="viviktaConsole" class="ng-cloak" ng-controller="ServersController" ng-init="listAllServers(0,'')">
<header id="header" class="ng-cloak">
    <ul class="header-inner ng-cloak">
        <li id="menu-trigger" data-trigger="#sidebar">
            <div class="line-wrap">
                <div class="line top"></div>
                <div class="line center"></div>
                <div class="line bottom"></div>
            </div>
        </li>
        <li class="logo hidden-xs">
            <a href="{{ url('servers') }}">Servers</a>
        </li>
    </ul>
    <div id="top-search-wrap">
        <input type="text">
        <i id="top-search-close">&times;</i>
    </div>
</header>
<section id="main" class="ng-cloak">
    <aside id="sidebar" class="ng-cloak">
        @include('sidebar');
    </aside>
    <section id="content" class="ng-cloak">
        <div class="container ng-cloak">
            <div class="block-header">
                <h2>Servers</h2>
                <ul class="actions" style="margin-right: 5%">
                    <li>
                        <a href="">
                            <button class="btn btn-info btn-icon-text waves-effect" ng-click="toggleServerForm()"><i class="fa fa-plus"></i> Add Server</button>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="dash-widgets ng-cloak" id="listServers" ng-if="serversFound">
                <div class="row ng-cloak">
                    <div class="col-md-3 col-sm-6 ng-cloak" ng-repeat="server in servers">
                        <div class="card">
                            <div class="card-header bgm-bluegray">
                                <h2><%server.serverName%></h2>
                                <ul class="actions">
                                    <li class="dropdown" >
                                        <a href="" data-toggle="dropdown">
                                            <i class="zmdi zmdi-more-vert"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li>
                                                <a href="" ng-click="viewServerDetails(server.serverId,server)">View Details</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <a class="project-description" href="">
                                <div class="card-body card-padding">
                                    <%server.serverDescription%>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card bgm-red" id="listServers" ng-if="!serversFound">
                <div class="row ng-cloak text-center">
                    <div class="col-md-12 ng-cloak">
                        <h2 style="color: white;">No Servers Found!</h2>
                    </div>
                </div>
            </div>
            <div class="card ng-cloak" id="addServers" style="display: none">
                <div class="card-header">
                    <h2>Configure Server</h2>
                </div>
                <div class="card-body card-padding">
                    <form name = "addServerForm">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Server Name:</label>
                            <input type="text" class="form-control" id="recipient-name1" ng-model="serverDetails.serverName" required>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Server Description:</label>
                            <textarea class="form-control" id="message-text1" ng-model="serverDetails.serverDescription" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Server Public IP:</label>
                            <textarea class="form-control" id="message-text1" ng-model="serverDetails.serverPublicIp" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Hosted With:</label>
                            <br/>
                            <input type="radio" ng-model="serverDetails.hostedWith" value="0" name="hostedWith">&nbsp;Digital Ocean
                            <input type="radio" ng-model="serverDetails.hostedWith" value="1" name="hostedWith">&nbsp;Amazon AWS EC2
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Login Username:</label>
                            <input type="text" class="form-control" id="recipient-name1" ng-model="serverDetails.serverUsername" required>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Password Type:</label>
                            <br/>
                            <input type="radio" ng-model="serverDetails.passwordType" value="0" name="passwordType">&nbsp;PEM Key File
                            <input type="radio" ng-model="serverDetails.passwordType" value="1" name="passwordType">&nbsp;Password
                        </div>
                        <div class="form-group" ng-if="serverDetails.passwordType == 0">
                            <label for="recipient-name" class="control-label">Choose PEM Key File:</label>
                            <input type="file" class="form-control" id="pemKeyFile" ng-model="serverDetails.serverPasswordPemFile">
                        </div>
                        <div class="form-group" ng-if="serverDetails.passwordType == 1">
                            <label for="recipient-name" class="control-label">Login Password:</label>
                            <input type="text" class="form-control" id="recipient-name1" ng-model="serverDetails.serverPassword">
                        </div>
                        <button class="btn btn-default waves-effect" ng-click="toggleServerForm()">Close</button>
                        <button class="btn btn-primary waves-effect" ng-click="addServer(serverDetails)" ng-disabled="addServerForm.$invalid">Add</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</section>
<footer id="footer" ng-controller="TimerController" class="ng-cloak">
    @include('footer');
    <div id="timer">
    </div>
</footer>
@include('scripts');
</body>
</html>