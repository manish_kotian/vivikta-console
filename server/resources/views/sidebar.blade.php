<div class="sidebar-inner c-overflow" ng-controller="SidemenuController">
    <div class="profile-menu">
        <a href="">
            <div class="profile-pic">
                <img src="{{asset('img/profile-pics/1.jpg')}}" alt="">
            </div>

            <div class="profile-info">
                Admin
                <i class="zmdi zmdi-arrow-drop-down"></i>
            </div>
        </a>

        <ul class="main-menu" style="display:none">
            <li>
                <a href=""><i class="zmdi zmdi-account"></i> View Profile</a>
            </li>
            <li>
                <a href=""><i class="zmdi zmdi-settings"></i> Settings</a>
            </li>
            <li>
                <a href=""><i class="zmdi zmdi-time-restore"></i> Logout</a>
            </li>

        </ul>
    </div>

    <ul class="main-menu">
        <li><a href="{{url('dashboard')}}"><i class="zmdi zmdi-home"></i> Dashboard</a></li>
        <li><a href="{{url('clients')}}"><i class="zmdi zmdi-account"></i> Vendors</a></li>
        <li><a href="{{url('servers')}}"><i class="zmdi zmdi-desktop-mac"></i> Servers</a></li>
        <li><a href="{{url('projects')}}"><i class="zmdi zmdi-album"></i> Projects</a></li>
        <li><a href="{{url('deployments')}}"><i class="zmdi zmdi-play-circle"></i> Deployments</a></li>
        <li>
            <a href="{{url('/')}}"><i class="zmdi zmdi-time-restore"></i> Logout</a>
        </li>
    </ul>
</div>