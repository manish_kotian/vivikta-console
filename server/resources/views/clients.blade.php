<!DOCTYPE html>
<html>
<head>
    <title>Vivikta Console | Vendors</title>
    @include('header')
</head>
<body ng-app="viviktaConsole" class="ng-cloak" ng-controller="VendorsController" ng-init="listAllVendors(0,'')">
<header id="header" class="ng-cloak">
    <ul class="header-inner ng-cloak">
        <li id="menu-trigger" data-trigger="#sidebar">
            <div class="line-wrap">
                <div class="line top"></div>
                <div class="line center"></div>
                <div class="line bottom"></div>
            </div>
        </li>
        <li class="logo hidden-xs">
            <a href="{{url('clients')}}p">Clients</a>
        </li>
    </ul>
    <div id="top-search-wrap">
        <input type="text">
        <i id="top-search-close">&times;</i>
    </div>
</header>
<section id="main" class="ng-cloak">
    <aside id="sidebar" class="ng-cloak">
        @include('sidebar');
    </aside>
    <section id="content" class="ng-cloak">
        <div class="container ng-cloak">
            <div class="block-header">
                <h2>Vendors</h2>
                <ul class="actions" style="margin-right: 5%">
                    <li>
                        <a href="">
                            <button class="btn btn-info btn-icon-text waves-effect" ng-click="toggleVendorsForm()"><i class="fa fa-plus"></i> Add Vendor</button>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="card" id="listVendors" ng-if="vendorsFound">
                <div class="table-responsive" style="min-height: 300px;">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="vendor in vendors">
                            <td><%$index+1%></td>
                            <td>
                                <%vendor.vendorName%>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card bgm-red" id="listVendors" ng-if="!vendorsFound">
                <div class="row ng-cloak text-center">
                    <div class="col-md-12 ng-cloak">
                        <h2 style="color: white;">No Vendors Found!</h2>
                    </div>
                </div>
            </div>
            <div class="card ng-cloak" id="addVendors" style="display: none">
                <div class="card-header">
                    <h2>Configure Vendors</h2>
                </div>
                <div class="card-body card-padding">
                    <form name = "addVendorsForm">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Name:</label>
                            <input type="text" class="form-control" id="recipient-name1" ng-model="vendorDetails.vendorName" required>
                        </div>
                        <button class="btn btn-default waves-effect" ng-click="toggleVendorsForm()">Close</button>
                        <button class="btn btn-primary waves-effect" ng-click="addVendor(vendorDetails)" ng-disabled="addVendorsForm.$invalid">Add</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</section>
<footer id="footer" ng-controller="TimerController" class="ng-cloak">
    @include('footer');
    <div id="timer">
    </div>
</footer>
@include('scripts');
</body>
</html>