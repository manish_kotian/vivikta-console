<!DOCTYPE html>
<html>
<head>
    <title>Vivikta Console | Projects</title>
    @include('header');
</head>
<body ng-app="viviktaConsole" class="ng-cloak" ng-controller="ProjectController" ng-init="listAllConfiguredApplication(0,'')">
<header id="header" class="ng-cloak">
    <ul class="header-inner ng-cloak">
        <li id="menu-trigger" data-trigger="#sidebar">
            <div class="line-wrap">
                <div class="line top"></div>
                <div class="line center"></div>
                <div class="line bottom"></div>
            </div>
        </li>
        <li class="logo hidden-xs">
            <a href="{{ url('projects') }}">Projects</a>
        </li>
    </ul>
    <div id="top-search-wrap">
        <input type="text">
        <i id="top-search-close">&times;</i>
    </div>
</header>
<section id="main" class="ng-cloak">
    <aside id="sidebar" class="ng-cloak">
        @include('sidebar');
    </aside>
    <section id="content" class="ng-cloak">
        <div class="container ng-cloak">
            <div class="block-header">
                <h2>Projects</h2>
                <ul class="actions" style="margin-right: 5%">
                    <li>
                        <a href="">
                            <button class="btn btn-info btn-icon-text waves-effect" ng-click="toggleProjectForm()"><i class="fa fa-plus"></i> Add Apps</button>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="card" id="closeProject" ng-if="projectsFound">
                <div class="table-responsive" style="min-height: 300px;">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="application in applications">
                            <td><%$index+1%></td>
                            <td>
                                <%application.appName%>
                            </td>
                            <td>
                                <%application.appDescription%>
                            </td>
                            <td>
                                            <span ng-if="application.appStatus == 0">
                                                <i class="zmdi zmdi-circle" style="color: green;"></i>
                                                Enabled
                                            </span>
                                <span ng-if="application.appStatus == 1">
                                                <i class="zmdi zmdi-circle" style="color: red;"></i>
                                                Disabled
                                            </span>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
                                        Action
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a ng-click="viewProjectDetails(application.appId,application)">View Details</a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card bgm-red" id="closeProject" ng-if="!projectsFound">
                <div class="row ng-cloak text-center">
                    <div class="col-md-12 ng-cloak">
                        <h2 style="color: white;">No Projects Found!</h2>
                    </div>
                </div>
            </div>
            <div class="card ng-cloak" id="project" style="display: none">
                <div class="card-header">
                    <h2>Configure Applications</h2>
                </div>
                <div class="card-body card-padding">
                    <form name = "addConfigurationForm">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">App Name:</label>
                            <input type="text" class="form-control" id="recipient-name1" ng-model="configuration.appName" required>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Description:</label>
                            <textarea class="form-control" id="message-text1" ng-model="configuration.appDescription" required></textarea>
                        </div>
                        <div class="form-group" ng-init="getAllDatabases()">
                            <label for="recipient-name" class="control-label">Database Type:</label>
                            <select class="form-control" ng-model="configuration.databaseType">
                                <option ng-repeat="database in databases" value="<%database.databaseId%>"><%database.databaseName%></option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Database Username:</label>
                            <input type="text" class="form-control" id="recipient-name1" ng-model="configuration.databaseUsername" required>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Database Password:</label>
                            <input type="text" class="form-control" id="recipient-name1" ng-model="configuration.databasePassword" required>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Gitlab Link:</label>
                            <textarea class="form-control" id="message-text1" ng-model="configuration.gitlabLink" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Gitlab User Name:</label>
                            <input type="text" class="form-control" id="recipient-name1" ng-model="configuration.gitlabUsername" required>
                        </div>
                        <button class="btn btn-default waves-effect" ng-click="toggleProjectForm()">Close</button>
                        <button class="btn btn-primary waves-effect" ng-click="configureApplications(configuration)" ng-disabled="addConfigurationForm.$invalid">Add</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</section>
<footer id="footer" ng-controller="TimerController" class="ng-cloak">
    @include('footer');
    <div id="timer">
    </div>
</footer>
@include('scripts');
</body>
</html>