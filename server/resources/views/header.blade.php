<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="{{ asset('vendors/bower_components/fullcalendar/dist/fullcalendar.min.css') }}" rel="stylesheet">
<link href="{{ asset('vendors/bower_components/animate.css/animate.min.css') }}" rel="stylesheet">
<link href="{{ asset('vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css') }}" rel="stylesheet">
<link href="{{ asset('vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css') }}" rel="stylesheet">
<link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">
<link href="{{ asset('css/app.min.1.css') }}" rel="stylesheet">
<link href="{{ asset('css/app.min.2.css') }}" rel="stylesheet">
<link href="{{ asset('css/custom-style.css') }}" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
<script type="text/javascript" src="{{ asset('js/demo.js') }}"></script>
<script src="{{ asset('bower_components/angular-busy/angular-busy.js') }}"></script>