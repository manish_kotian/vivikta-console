#!/usr/bin/bash

old_version="$1"
new_version="$2"
git_lab_https="$3"
apk_path="$4"
app_name="$5"      
program_path="$6"
git_pull_directory="$7"

keystore_name="vivikta"

git_project_directory="$git_pull_directory/$app_name"

android_release_path="$program_path/platforms/android/build/outputs/apk/"
	
js_directory="$git_project_directory/www/js/" 
 
suffix="js"

js_backup="$git_project_directory/jsbackup/" 
 
uglifyd_js="$git_project_directory/uglifyd_js"

status =0



buildandroid()
{
	echo "build android enter--------->"
	prefix="android-release-unsigned"

	echo "built"
	cd $android_release_path
	for f in *
	do
		releasename=`echo "$f" | cut -d '.' -f1`
		if [ $releasename = $prefix ]
		then
			echo "moving $f"
			cp $android_release_path"${f%.apk}".apk $program_path/"$app_name"_"$new_version"_unsigned.apk
		fi
	done			
	cd $program_path/
	jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore "$keystore_name".keystore "$app_name"_"$new_version"_unsigned.apk -storepass vivikta alias_name
	retval=$?
	if [ $retval -ne 0 ]
	then
		filename="config.xml"
		cd $program_path/
		for i in *
		do
			if [ -f "$i"  ]
			then
			if [ $i = $filename ]
			then 
					sed -i 's/version="'$new_version'"/version="'$old_version'"/' $i
			fi
			fi
		done
		cd $program_path/
		rm -R -f "$app_name"_"$new_version"_unsigned.apk
		rm -R -f www
		mv -f www1 www
		cd $apk_path/
		status=5
		echo "$status" > $app_name.txt
		cd $git_pull_directory/
		rm -R -f $app_name
		echo "jarsigner failed"
		exit 1
	fi
	zipalign -v 4 "$app_name"_"$new_version"_unsigned.apk "$app_name"_"$new_version".apk
	retval=$?
	if [ $retval -ne 0 ]
	then
		filename="config.xml"
		cd $program_path/
		for i in *
		do
			if [ -f "$i"  ]
			then
			if [ $i = $filename ]
			then 
					sed -i 's/version="'$new_version'"/version="'$old_version'"/' $i
			fi
			fi
		done
		cd $program_path/
		rm -R -f "$app_name"_"$new_version"_unsigned.apk
		rm -R -f "$app_name"_"$new_version".apk
		rm -R -f www
		mv -f www1 www
		cd $apk_path/
		status=6
		echo "$status" > $app_name.txt
		cd $git_pull_directory/
		rm -R -f $app_name
		echo "zipalign failed"
		exit 1
	fi
	cp "$app_name"_"$new_version".apk $apk_path/
	cd $program_path/
	rm "$app_name"_"$new_version"_unsigned.apk
	rm -R -f www
	mv -f www1 www
}

cd $program_path/
buildandroid

cd $apk_path/
echo "$status" > $app_name.txt


cd $git_pull_directory/
#rm -R -f $app_name



		
	











