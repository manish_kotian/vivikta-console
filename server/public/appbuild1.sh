#!/usr/bin/bash

old_version="$1"
new_version="$2"
git_lab_https="$3"
apk_path="$4"
app_name="$5"      
program_path="$6"
git_pull_directory="$7"

keystore_name="vivikta"

git_project_directory="$git_pull_directory/$app_name"

android_release_path="$program_path/platforms/android/build/outputs/apk/"
	
js_directory="$git_project_directory/www/js/" 
 
suffix="js"

js_backup="$git_project_directory/jsbackup/" 
 
uglifyd_js="$git_project_directory/uglifyd_js"

base()
{
	cd $git_project_directory/
	mkdir jsbackup
	chmod -R 777 jsbackup
	for d in *
	do
		if [ -d "$git_project_directory/uglifyd_js" ]
		then 
			break
		else
			mkdir uglifyd_js
        		chmod -R 777 uglifyd_js
		fi
	done
	cd $1
	for f in *
	do
			extension=`echo "$f" | cut -d '.' -f2`
			if [ -f "$1$f" ]
	 		then
				if [ $extension = $suffix ]
				then
					cd $1
					cp -fR $f $uglifyd_js/"${f%.js}".js
					cp -fR $f $js_backup"${f%.js}".js
					printf "uglifying $f\n"
					uglify "$f"
					mv  $js_backup"${f%.js}"1.js $uglifyd_js/"${f%.js}".js
	 			else
					cd $apk_path/
					status=3
					echo "$status" > $app_name.txt
					cd $git_pull_directory/
					rm -R -f $app_name
					exit 1
				fi
			elif [ -d "$1$f" ]
			then
				uglifydir $js_directory$f/ $f
			fi
		
	done
}



uglify()
{
	
	 uglifyjs $js_backup"${f%.js}".js > $js_backup"${f%.js}"1.js

}

uglifydir()
{
	cd $1
	for f in *
	do
			extension=`echo "$f" | cut -d '.' -f2`
			if [ -f "$1$f" ]
	 		then
				if [ $extension = $suffix ]
				then
					test -d $uglifyd_js/$2 || mkdir $uglifyd_js/$2 && cp -fR $f $uglifyd_js/$2/"${f%.js}".js
					cp -fR $f $js_backup"${f%.js}".js
					printf "uglifying $f\n"
					uglify "$f"
					mv  $js_backup"${f%.js}"1.js $uglifyd_js/$2/"${f%.js}".js
					
	 			else
					cd $apk_path/
					status=3
					echo "$status" > $app_name.txt
					cd $git_pull_directory/
					rm -R -f $app_name
					exit 1
				fi
			elif [ -d "$1$f" ]
			then
				uglifydir $1$f/ $f
			fi
	done
}
version()
{
	filename="config.xml"
	#cd $python_code_directory
	#old_version=$(python modify_version.py "$git_project_directory/config.xml")
	#printf "\n***CURRENT VERSION IS: $old_version\n"
	#printf "\n"
       # read -p  "***ENTER THE NEW VERSION NUMBER: " new_version
        #while  [[ ! $new_version =~ ^[0-9]\.?[.][0-9]\.?[.][0-9]$ ]] 
	#do
		#unset new_version
		#printf "\n"
        	#read -p  "***PLEASE ENTER THE NEW VERSION NUMBER IN THE FORMAT (X.X.X): " new_version
    	#done
	cd $program_path/
	for i in *
	do
		if [ -f "$i"  ]
		then
			if [ $i = $filename ]
			then 
					sed -i 's/version="'$old_version'"/version="'$new_version'"/' $i

			fi	
		fi
	done
}

move_files()
{
	echo "move files enter---------> " >> "test.txt"
	cd $program_path/
	rm -Rf www1
	mv -f www www1
	cd $git_project_directory/
	for d in *
	do
		if [ $d = "www" ] || [ $d = "uglifyd_js" ];
		then
			cp -fR $d $program_path/
		fi
	done
	cd  $program_path/www/
	rm -R -f js
	cd $program_path/
	mv -f uglifyd_js js
	mv -f js $program_path/www/
	echo "move files exit--------->"
	
	
}




cd $git_pull_directory/
rm -Rf $app_name
printf "\n*********************GIT CLONE************************\n"
printf "\n***CLONING TO FOLDER:  $app_name\n"
printf "\n"
status=0
git clone $git_lab_https || sleep 1m; kill $! 
retval=$?
cd $git_pull_directory/
for d in *
do
	if [ -d "$app_name" ] 
	then 
		if [ $retval -ne 0 ]
		then
			:
		else
			cd $apk_path/
			status=1
			echo "$status" > $app_name.txt
			cd $git_pull_directory/
			rm -R -f $app_name
			exit 1
		fi
			
	else 
			cd $apk_path/
			status=1
			echo "$status" > $app_name.txt
			cd $git_pull_directory/
			rm -R -f $app_name
			exit 1

	fi
done

cd $git_project_directory/
printf "\n*******************UGLIFYING THE JAVASCRIPT FILES**************************\n"
base "$js_directory"
cd $git_project_directory/
rm -R -f jsbackup
printf "\n*****************REPLACING VERSION NUMBER*************\n"
version
printf "\n***************BUILDING SIGNED APK FILE*************\n"

move_files

cd $program_path/
rm -rf Input.txt
touch Input.txt
echo $program_path > Input.txt




		
	











