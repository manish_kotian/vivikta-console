app
.controller('VendorsController',function($scope,$http,TokenVerifierService){
	$scope.authToken = localStorage.getItem("viviktaConsole_token");
	var configuration = new Configuration();

	$scope.isVendorFormOpened = false;
	$scope.toggleVendorsForm = function(){
		if($scope.isVendorFormOpened == false){
			$("#addVendors").slideDown("slow");
			$("#listVendors").slideUp("slow");
			$scope.isVendorFormOpened = true;
		}
		else{
			$("#listVendors").slideDown("slow");
			$("#addVendors").slideUp("slow");
			$scope.isVendorFormOpened = false;
		}
	};

	//function to get the list of all vendors
	$scope.listAllVendors = function(paginateFlag,url){
		if(paginateFlag == 0){
			$scope.url = configuration.BASE_URL+configuration.LIST_VENDORS_URL;
		}
		else{
			$scope.url = url;
		}
		$scope.ajaxPromise = $http({
			method : "GET",
			url : $scope.url,
			params : {
				token : $scope.authToken,
				filterType : 0
			}
		}).then(function success(response){
			writeLogsToFile('info',JSON.stringify(response),2);
			if(response.status == configuration.HTTP_STATUS_CODE){
				if(response.data.errorCode == configuration.LIST_VENDORS_SUCCESS_CODE && response.data.statusText == configuration.SUCCESS_STATUS_TEXT){
					$scope.lastPage = response.data.data.lastPage;
		            $scope.showPagination = true;
		            if($scope.lastPage == "NULL"){
		                $scope.showPagination = false;
		                if(response.data.data.data.length > 0){
		                    $scope.vendors = response.data.data.data;
		                    $scope.vendorsFound = true;
		                }
		                else{
		                    $scope.vendorsFound = false;
		                }
		            }
		            else{
		                if(response.data.data.data.length > 0){
		                    $scope.vendors = response.data.data.data;
		                    $scope.vendorsFound = true;
		                }
		                else{
		                    $scope.vendorsFound = false;
		                }
		            }
		            $scope.previousPageUrl = response.data.data.previousPageUrl;
		            $scope.nextPageUrl = response.data.data.nextPageUrl;
		            $scope.currentPage = response.data.data.currentPage;
				}
				else{
					$scope.vendorsFound = false;
					TokenVerifierService.verifyToken(response.data.errorCode,response.data.statusText,response.data.data);
				}
			}
			else{
				$scope.vendorsFound = false;
				console.log("no data found");
			}
		},function failure(){
			$scope.vendorsFound = false;
			console.log("no data found");
		});
	};

	//function to add a vendor
	$scope.addVendor = function(vendorDetails){
		$scope.ajaxPromise = $http({
			method : "POST",
			url : configuration.BASE_URL+configuration.ADD_VENDOR_URL,
			params : {
				token : $scope.authToken,
				vendorName : vendorDetails.vendorName
			}
		}).then(function success(response){
			writeLogsToFile('info',JSON.stringify(response),2);
			if(response.status == configuration.HTTP_STATUS_CODE){
				if(response.data.errorCode == configuration.ADD_VENDOR_SUCCESS_CODE && response.data.statusText == configuration.SUCCESS_STATUS_TEXT){
					notify('Vendor added successfully!', 'success');
					$scope.vendorDetails = {};
					$scope.toggleVendorsForm();
					$scope.listAllVendors(0,'');
				}
				else{
					notify('Vendor could not be added!', 'danger');
					TokenVerifierService.verifyToken(response.data.errorCode,response.data.statusText,response.data.data);
				}
			}
			else{
				notify('Vendor could not be added!', 'danger');
			}
		},function failure(){
			notify('Server error occured!', 'danger');
		});
	};
})