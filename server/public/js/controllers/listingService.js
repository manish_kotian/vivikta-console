app
.factory('ListingService', [ '$http','$q', function($http,$q){
    var configuration = new Configuration();
    var listingService = {};
    listingService.getData = function(AUTH_TOKEN,URL,ERROR_CODE,FILTER_TYPE){
        var deferred = $q.defer();
        return $http({
            method : "GET",
            url : URL,
            params : {
                token : AUTH_TOKEN,
                filterType : FILTER_TYPE
            }
        }).then(function success(response){
            writeLogsToFile("info","getData Response: "+JSON.stringify(response),2);
            if(response.status == configuration.HTTP_STATUS_CODE){
                if(response.data.errorCode == ERROR_CODE && response.data.statusText == configuration.SUCCESS_STATUS_TEXT && response.data.data != '' && response.data.data != null && typeof(response.data.data) !== undefined){
                    deferred.resolve(response.data.data);
                    return deferred.promise;
                }
                else{
                    deferred.reject('No data found');
                    return deferred.promise;
                }
            }
            else{
                deferred.reject('Encountered server error!');
                return deferred.promise;
            }
        },function failure(){
            deferred.reject('Encountered server error!');
            return deferred.promise;
        });
    }
    return listingService;
}])