app
.controller('ProjectController',function($scope,$http,TokenVerifierService,ListingService){
	$scope.authToken = localStorage.getItem("viviktaConsole_token");
	var configuration = new Configuration();

	$scope.isProjectFormOpened = false;
	$scope.toggleProjectForm = function(){
		if($scope.isProjectFormOpened == false){
			$("#project").slideDown("slow");
			$("#closeProject").slideUp("slow");
			$scope.isProjectFormOpened = true;
		}
		else{
			$("#closeProject").slideDown("slow");
			$("#project").slideUp("slow");
			$scope.isProjectFormOpened = false;
		}
	};	

	$scope.listAllConfiguredApplication =function(paginateFlag,url){
		if(paginateFlag == 0){
			$scope.url = configuration.BASE_URL+configuration.LIST_CONFIGURATIONS_URL;
		}
		else{
			$scope.url = url;
		}
		$scope.ajaxPromise = $http({
			method : "GET",
			url : $scope.url,
			params : {
				token : $scope.authToken,
				filterType : 0
			}
		}).then(function success(response){
			writeLogsToFile('info','ExecutionController-->'+JSON.stringify(response),2);
			if(response.status == configuration.HTTP_STATUS_CODE){
				if(response.data.errorCode == configuration.LIST_CONFIGURATIONS_SUCCESS_CODE && response.data.statusText == configuration.SUCCESS_STATUS_TEXT){
					$scope.lastPage = response.data.data.lastPage;
		            $scope.showPagination = true;
		            if($scope.lastPage == "NULL"){
		                $scope.showPagination = false;
		                if(response.data.data.data.length > 0){
		                    $scope.applications = response.data.data.data;
		                    $scope.projectsFound = true;
		                }
		                else{
		                    $scope.projectsFound = false;
		                }
		            }
		            else{
		                LogService.infoLog(JSON.stringify(response));
		                if(response.data.data.data.length > 0){
		                    $scope.applications = response.data.data.data;
		                    $scope.projectsFound = true;
		                }
		                else{
		                    $scope.projectsFound = false;
		                }
		            }
		            $scope.previousPageUrl = response.data.data.previousPageUrl;
		            $scope.nextPageUrl = response.data.data.nextPageUrl;
		            $scope.currentPage = response.data.data.currentPage;
				}
				else{
					$scope.projectsFound = false;
					TokenVerifierService.verifyToken(response.data.errorCode,response.data.statusText,response.data.data);
				}
			}
			else{
				$scope.projectsFound = false;
				console.log("no data found");
			}
		},function failure(){
			$scope.projectsFound = false;
			console.log("no data found");
		});
	};

	//function to get the list of all databases
	$scope.getAllDatabases = function(){
		ListingService.getData($scope.authToken,configuration.BASE_URL+configuration.LIST_DATABASES_URL,configuration.LIST_DATABASES_SUCCESS_CODE,0).then(function success(databasesList){
			if(databasesList.length > 0){
				$scope.databases = databasesList;
				$scope.databasesFound = true;
			}
			else{
				$scope.databasesFound = false;
			}
		},function failure(errorMessage){
			$scope.databasesFound = false;
		});
	};

	$scope.configureApplications = function(configurationData){
		$scope.ajaxPromise = $http({
			method : "POST",
			url : configuration.BASE_URL+configuration.ADD_CONFIGURATION_URL,
			params : {
				token : $scope.authToken,
				appName : configurationData.appName,
				appDescription : configurationData.appDescription,
				appDirectoryPath : configurationData.appDirectoryPath,
				appVirtualHostName : configurationData.appVirtualHostName,
				appHostedOn : configurationData.appHostedOn,
				databaseId : configurationData.databaseType,
				databaseUsername : configurationData.databaseUsername,
				databasePassword : configurationData.databasePassword,
				gitlabLink : configurationData.gitlabLink,
				gitlabUsername : configurationData.gitlabUsername
			}
		}).then(function success(response){
			writeLogsToFile('info','ExecutionController-->'+JSON.stringify(response),2);
			if(response.status == configuration.HTTP_STATUS_CODE){
				if(response.data.errorCode == configuration.ADD_CONFIGURATION_SUCCESS_CODE && response.data.statusText == configuration.SUCCESS_STATUS_TEXT){
					notify('Project created successfully!', 'success');
					$scope.toggleProjectForm();
					$scope.listAllConfiguredApplication(0,'');
				}
				else{
					notify('Project could not be created!', 'danger');
					TokenVerifierService.verifyToken(response.data.errorCode,response.data.statusText,response.data.data);
				}
			}
			else{
				notify('Project could not be created!', 'danger');
			}
		},function failure(){
			notify('Server error occured!', 'danger');
		});
	}

	$scope.viewProjectDetails = function(appId,appData){
		localStorage.setItem("console_appDetails",JSON.stringify(appData));
		window.location.href = "viewProjectDetails";
	};

	$scope.loadProjectDetailsByAppId = function(){
		$scope.projectDetails = JSON.parse(localStorage.getItem("console_appDetails"));
	};

	//function to navigate to project settings
	$scope.projectSettings = function(appId,appData){
		localStorage.setItem("console_appDetails",JSON.stringify(appData));
		window.location.href = "projectSettings";
	};

	$scope.updateConfiguration = function(editConfigurationData){
		$scope.ajaxPromise = $http({
			method : "PUT",
			url : configuration.BASE_URL+configuration.UPDATE_CONFIGURATION_URL,
			params : {
				token : $scope.authToken,
				appId : editConfigurationData.appId,
				appName : editConfigurationData.appName,
				appDescription : editConfigurationData.appDescription,
				gitlabLink : editConfigurationData.gitlabLink
			}
		}).then(function success(response){
			writeLogsToFile('info','ExecutionController-->'+JSON.stringify(response),2);
			if(response.status == configuration.HTTP_STATUS_CODE){
				if(response.data.errorCode == configuration.UPDATE_CONFIGURATION_SUCCESS_CODE && response.data.statusText == configuration.SUCCESS_STATUS_TEXT){
					notify('configuration updated succssfully', 'success');
					$scope.listAllConfiguredApplication(0,'');
				}
				else{
					notify('configuration update Failed', 'danger');
					TokenVerifierService.verifyToken(response.data.errorCode,response.data.statusText,response.data.data);
				}
			}
			else{
				notify('configuration update Failed', 'danger');
			}
		},function failure(){
			notify('configuration update Failed --> Server Error', 'danger');
		});
	}
})