var serverFormData = new FormData();
app
.service('AddServerService', ['$http','$q', function ($http,$q) {
    this.add = function(authToken,serverDetails,serverPassword,uploadUrl){
        var deferred = $q.defer();
        return $http.post(uploadUrl, serverFormData, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined},
            params:{
              	token : authToken,
              	serverName : serverDetails.serverName,
              	serverDescription : serverDetails.serverDescription,
              	serverPublicIp : serverDetails.serverPublicIp,
              	hostedWith : serverDetails.hostedWith,
              	serverUsername : serverDetails.serverUsername,
              	serverPasswordType : serverDetails.passwordType,
              	serverPassword : serverPassword
            }
        }).then(function success(response){
            writeLogsToFile('info',JSON.stringify(response),2);
            if(response.data.errorCode == configuration.ADD_SERVER_SUCCESS_CODE && response.data.statusText == configuration.SUCCESS_STATUS_TEXT){
                deferred.resolve(response);
                return deferred.promise;
            }
            else{
                deferred.reject();
                return deferred.promise;
            }
        },function failure(){
            deferred.reject();
            return deferred.promise;
        })
    }
}])
.controller('ServersController',function($scope,$http,TokenVerifierService,AddServerService){
	$scope.authToken = localStorage.getItem("viviktaConsole_token");
	var configuration = new Configuration();

	$scope.isServerFormOpened = false;
	$scope.toggleServerForm = function(){
		if($scope.isServerFormOpened == false){
			$("#addServers").slideDown("slow");
			$("#listServers").slideUp("slow");
			$scope.isServerFormOpened = true;
		}
		else{
			$("#listServers").slideDown("slow");
			$("#addServers").slideUp("slow");
			$scope.isServerFormOpened = false;
		}
	};

	//function to get the list of all servers
	$scope.listAllServers = function(paginateFlag,url){
		if(paginateFlag == 0){
			$scope.url = configuration.BASE_URL+configuration.LIST_SERVERS_URL;
		}
		else{
			$scope.url = url;
		}
		$scope.ajaxPromise = $http({
			method : "GET",
			url : $scope.url,
			params : {
				token : $scope.authToken,
				filterType : 0
			}
		}).then(function success(response){
			writeLogsToFile('info','ExecutionController-->'+JSON.stringify(response),2);
			if(response.status == configuration.HTTP_STATUS_CODE){
				if(response.data.errorCode == configuration.LIST_SERVERS_SUCCESS_CODE && response.data.statusText == configuration.SUCCESS_STATUS_TEXT){
					$scope.lastPage = response.data.data.lastPage;
		            $scope.showPagination = true;
		            if($scope.lastPage == "NULL"){
		                $scope.showPagination = false;
		                if(response.data.data.data.length > 0){
		                    $scope.servers = response.data.data.data;
		                    $scope.serversFound = true;
		                }
		                else{
		                    $scope.serversFound = false;
		                }
		            }
		            else{
		                LogService.infoLog(JSON.stringify(response));
		                if(response.data.data.data.length > 0){
		                    $scope.servers = response.data.data.data;
		                    $scope.serversFound = true;
		                }
		                else{
		                    $scope.serversFound = false;
		                }
		            }
		            $scope.previousPageUrl = response.data.data.previousPageUrl;
		            $scope.nextPageUrl = response.data.data.nextPageUrl;
		            $scope.currentPage = response.data.data.currentPage;
				}
				else{
					$scope.serversFound = false;
					TokenVerifierService.verifyToken(response.data.errorCode,response.data.statusText,response.data.data);
				}
			}
			else{
				$scope.serversFound = false;
				console.log("no data found");
			}
		},function failure(){
			$scope.serversFound = false;
			console.log("no data found");
		});
	};

	//function to add a server
	$scope.addServer = function(serverDetails){
		if(serverDetails.serverPasswordType == 0){
			serverFormData.append('serverPasswordPemFile',$("#pemKeyFile")[0].files);
			$scope.serverPassword = '';
		}
		else{
			$scope.serverPassword = serverDetails.serverPassword;
		}
		$scope.uploadUrl = configuration.BASE_URL+configuration.ADD_SERVER_URL;
		AddServerService.add($scope.authToken,serverDetails,$scope.serverPassword,$scope.uploadUrl).then(function success(response){
			serverFormData.delete('serverPasswordPemFile');
			notify('Server added successfully!', 'success');
			$scope.toggleServerForm();
			$scope.listAllServers(0,'');
			$scope.serverDetails = {};
		},function failure(){
			notify('Could not add the server!', 'danger');
		});
	};
})