app
.controller('DeploymentsController',function($scope,$http,TokenVerifierService,ListingService){
	$scope.authToken = localStorage.getItem("viviktaConsole_token");
	var configuration = new Configuration();

	$scope.isDeploymentFormOpened = false;
	$scope.toggleDeploymentsForm = function(){
		if($scope.isDeploymentFormOpened == false){
			$("#addDeployments").slideDown("slow");
			$("#listDeployments").slideUp("slow");
			$scope.isDeploymentFormOpened = true;
		}
		else{
			$("#listDeployments").slideDown("slow");
			$("#addDeployments").slideUp("slow");
			$scope.isDeploymentFormOpened = false;
		}
	};

	//function to get the list of all deployments
	$scope.listAllDeployments = function(paginateFlag,url){
		if(paginateFlag == 0){
			$scope.url = configuration.BASE_URL+configuration.LIST_DEPLOYMENTS_URL;
		}
		else{
			$scope.url = url;
		}
		$scope.ajaxPromise = $http({
			method : "GET",
			url : $scope.url,
			params : {
				token : $scope.authToken,
				filterType : 0
			}
		}).then(function success(response){
			writeLogsToFile('info',JSON.stringify(response),2);
			if(response.status == configuration.HTTP_STATUS_CODE){
				if(response.data.errorCode == configuration.LIST_DEPLOYMENTS_SUCCESS_CODE && response.data.statusText == configuration.SUCCESS_STATUS_TEXT){
					$scope.lastPage = response.data.data.lastPage;
		            $scope.showPagination = true;
		            if($scope.lastPage == "NULL"){
		                $scope.showPagination = false;
		                if(response.data.data.data.length > 0){
		                    $scope.deployments = response.data.data.data;
		                    $scope.deploymentsFound = true;
		                }
		                else{
		                    $scope.deploymentsFound = false;
		                }
		            }
		            else{
		                if(response.data.data.data.length > 0){
		                    $scope.deployments = response.data.data.data;
		                    $scope.deploymentsFound = true;
		                }
		                else{
		                    $scope.deploymentsFound = false;
		                }
		            }
		            $scope.previousPageUrl = response.data.data.previousPageUrl;
		            $scope.nextPageUrl = response.data.data.nextPageUrl;
		            $scope.currentPage = response.data.data.currentPage;
				}
				else{
					$scope.deploymentsFound = false;
					TokenVerifierService.verifyToken(response.data.errorCode,response.data.statusText,response.data.data);
				}
			}
			else{
				$scope.deploymentsFound = false;
				console.log("no data found");
			}
		},function failure(){
			$scope.deploymentsFound = false;
			console.log("no data found");
		});
	};

	$scope.getVendorsList = function(){
		ListingService.getData($scope.authToken,configuration.BASE_URL+configuration.LIST_VENDORS_URL,configuration.LIST_VENDORS_SUCCESS_CODE,1).then(function success(vendorsList){
			if(vendorsList.length > 0){
				$scope.vendors = vendorsList;
				$scope.vendorsFound = true;
			}
			else{
				$scope.vendorsFound = false;
			}
		},function failure(errorMessage){
			$scope.vendorsFound = false;
		});
	};

	$scope.getServersList = function(){
		ListingService.getData($scope.authToken,configuration.BASE_URL+configuration.LIST_SERVERS_URL,configuration.LIST_SERVERS_SUCCESS_CODE,1).then(function success(serversList){
			if(serversList.length > 0){
				$scope.servers = serversList;
				$scope.serversFound = true;
			}
			else{
				$scope.serversFound = false;
			}
		},function failure(errorMessage){
			$scope.serversFound = false;
		});
	};

	$scope.getProjectsList = function(){
		ListingService.getData($scope.authToken,configuration.BASE_URL+configuration.LIST_CONFIGURATIONS_URL,configuration.LIST_CONFIGURATIONS_SUCCESS_CODE,1).then(function success(projectsList){
			if(projectsList.length > 0){
				$scope.projects = projectsList;
				$scope.projectsFound = true;
			}
			else{
				$scope.projectsFound = false;
			}
		},function failure(errorMessage){
			$scope.projectsFound = false;
		});
	};

	//function to add a deployment
	$scope.addDeployment = function(deploymentDetails){
		$scope.ajaxPromise = $http({
			method : "POST",
			url : configuration.BASE_URL+configuration.ADD_DEPLOYMENT_URL,
			params : {
				token : $scope.authToken,
				deploymentName : deploymentDetails.deploymentName,
				deploymentDescription : deploymentDetails.deploymentDescription,
				deploymentDirectoryPath : deploymentDetails.deploymentDirectoryPath,
				deploymentUrl : deploymentDetails.deploymentUrl,
				deploymentVirtualHostName : deploymentDetails.deploymentVirtualHostName,
				deploymentVendorId : deploymentDetails.deploymentVendorId,
				deploymentHostedOn : deploymentDetails.deploymentHostedOn,
				deploymentProjectId : deploymentDetails.deploymentProjectId
			}
		}).then(function success(response){
			writeLogsToFile('info',JSON.stringify(response),2);
			if(response.status == configuration.HTTP_STATUS_CODE){
				if(response.data.errorCode == configuration.ADD_DEPLOYMENT_SUCCESS_CODE && response.data.statusText == configuration.SUCCESS_STATUS_TEXT){
					notify('Deployment added successfully!', 'success');
					$scope.deploymentDetails = {};
					$scope.toggleDeploymentsForm();
					$scope.listAllDeployments(0,'');
				}
				else{
					notify('Deployment could not be added!', 'danger');
					TokenVerifierService.verifyToken(response.data.errorCode,response.data.statusText,response.data.data);
				}
			}
			else{
				notify('Deployment could not be added!', 'danger');
			}
		},function failure(){
			notify('Server error occured!', 'danger');
		});
	};

	//function to view the details of a deployment
	$scope.viewDeploymentDetails = function(deploymentDetails){
		localStorage.setItem("console_deploymentDetails",JSON.stringify(deploymentDetails));
		window.location.href = "viewDeploymentDetails";
	};

	//function to load the details of a deployment
	$scope.loadDeploymentDetails = function(){
		$scope.deploymentDetails = JSON.parse(localStorage.getItem("console_deploymentDetails"));
	};

	//function to get the settings of a deployment
	$scope.deploymentSettings = function(deploymentDetails){
		localStorage.setItem("console_deploymentDetails",JSON.stringify(deploymentDetails));
		window.location.href = "deploymentSettings";
	};

	//function to load the settings of a deployment
	$scope.loadDeploymentSettings = function(){
		$scope.deploymentDetails = JSON.parse(localStorage.getItem("console_deploymentDetails"));
		if($scope.deploymentDetails.deploymentStatus == 0){
			$("#deploymentEnableStatus input:checkbox").prop('checked',true);
		}
		else{
			$("#deploymentEnableStatus input:checkbox").prop('checked',false);
		}
	};

	$("#deploymentEnableStatus input:checkbox").on('change',function(){
		if($("#deploymentEnableStatus input:checkbox").is(':checked')){
			$scope.updateDeploymentStatus(0);
		}
		else{
			$scope.updateDeploymentStatus(1);
		}
	});

	$scope.updateDeploymentStatus = function(status){
		$scope.deploymentDetails = JSON.parse(localStorage.getItem("console_deploymentDetails"));
		$scope.ajaxPromise = $http({
			method : "POST",
			url : configuration.BASE_URL + configuration.UPDATE_DEPLOYMENT_STATUS_URL,
			params : {
				token : $scope.authToken,
				deploymentId : $scope.deploymentDetails.deploymentId,
				deploymentStatus : status
			}
		}).then(function success(response){
			writeLogsToFile('info',JSON.stringify(response),2);
			if(response.status == configuration.HTTP_STATUS_CODE){
				if(response.data.errorCode == configuration.UPDATE_DEPLOYMENT_STATUS_SUCCESS_CODE && response.data.statusText == configuration.SUCCESS_STATUS_TEXT){
					notify('Deployment status updated successfully!','success');
				}
				else{
					notify('Deployment status update could not be done!','danger');
				}
			}
			else{
				notify('Deployment status update could not be done!','danger');
			}
		},function failure(){
			writeLogsToFile('error','Update deployment status error!',2);
			notify('Deployment status update could not be done!','danger');
		});
	}
})