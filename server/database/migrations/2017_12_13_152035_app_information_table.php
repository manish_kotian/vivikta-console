<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AppInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_information_table', function (Blueprint $table) {
            $table->increments('id');
            $table->string('appId');
            $table->string('appName');
            $table->text('appDescription');
            $table->string('databaseId');
            $table->string('databaseUsername');
            $table->string('databasePassword');
            $table->string('gitlabLink');
            $table->string('gitlabUsername');
            $table->string('appStatus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('app_information_table');
    }
}
