<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ServerEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('server_event_table', function (Blueprint $table) {
            $table->increments('id');
            $table->string('eventId');
            $table->string('serverId');
            $table->string('serverType');
            $table->text('maximumCpuUsage');
            $table->text('minimumCpuUsage');
            $table->text('cpuTimestamp');
            $table->bigInteger('cpuTimestampInt');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('server_event_table');
    }
}
