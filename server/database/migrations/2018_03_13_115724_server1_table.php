<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Server1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('server_table', function (Blueprint $table) {
            $table->increments('id');
            $table->string('serverId');
            $table->string('serverName');
            $table->string('serverDescription');
            $table->string('serverPublicIp');
            $table->string('hostedWith');
            $table->string('serverUsername');
            $table->string('serverPasswordType');
            $table->string('serverPassword');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('server_table');
    }
}
