<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ServerMemoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('server_memory_table', function (Blueprint $table) {
            $table->increments('id');
            $table->string('memoryId');
            $table->string('serverId');
            $table->string('serverType');
            $table->text('memoryDetails');
            $table->text('diskUsageDetails');
            $table->bigInteger('memoryTimestamp');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('server_memory_table');
    }
}
