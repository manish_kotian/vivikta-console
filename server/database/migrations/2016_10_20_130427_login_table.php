<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LoginTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('login_table', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('emailId');
            $table->string('password',60);
            $table->string('typeofuser');
            $table->string('loginId');
            $table->string('status');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('login_table');
    }
}
