<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeploymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deployments_table', function (Blueprint $table) {
            $table->increments('id');
            $table->string('deploymentId');
            $table->string('deploymentName');
            $table->text('deploymentDescription');
            $table->string('deploymentUrl');
            $table->string('deploymentDirectoryPath');
            $table->string('deploymentVirtualHostName');
            $table->string('deploymentHostedOn');
            $table->string('deploymentProjectId');
            $table->string('deploymentStatus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('deployments_table');
    }
}
