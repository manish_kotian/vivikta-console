<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AppBuildTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_build_table', function (Blueprint $table) {
            $table->increments('id');
            $table->string('buildId');
            $table->string('appId');
            $table->string('incrementType');
            $table->string('versionNumber')->nullable();
            $table->string('gitlabLink');
            $table->string('gitlabUsername');
            $table->string('gitlabPassword');
            $table->string('apkPath')->nullable();
            $table->string('logFilePath')->nullable();
            $table->string('buildStatus')->nullable();
            $table->string('buildString')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('app_build_table');
    }
}
