<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CpuDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cpu_data_table', function (Blueprint $table) {
            $table->increments('id');
            $table->string('logId');
            $table->string('serverId');
            $table->string('serverType');
            $table->text('cpuDetails');
            $table->text('memoryDetails');
            $table->text('diskUsageDetails');
            $table->bigInteger('cpuTimestampInt');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cpu_data_table');
    }
}
