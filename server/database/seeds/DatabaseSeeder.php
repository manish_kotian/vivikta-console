<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        Model::unguard();
        $this->call('UsersTableSeeder');


        Model::reguard();
    }
}
class UsersTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('login_table');
        $users = array(
            ['name' => 'Vivikta', 'emailId' => 'support@vivikta.in', 'password' => \Illuminate\Support\Facades\Hash::make('vivikta'),'typeofuser'=>'0','status'=>'Active','loginId' => '12345']
        );

// Loop through each user above and create the record for them in the database
        foreach ($users as $user)
        {
            \App\User::create($user);
        }

        Model::reguard();

    }
}












