<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServerMemory extends Model
{
    protected $table = "server_memory_table";
}
