<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
class AuthUser
{
    protected $errorCode = '';
    public function authenticateUser()
    {
        try
        {
            //check for token present with parameters
            if (! $user = JWTAuth::parseToken()->authenticate())
            {
                $this->errorCode = "404";
                return $this->errorCode;
            }
            else{
                $this->errorCode = "400";
                return $this->errorCode;
            }
        }
        catch (TokenExpiredException $e)
        {
            $this->errorCode = "405";
            return $this->errorCode;
        }
        catch (TokenInvalidException $e)
        {
            $this->errorCode = "406";
            return $this->errorCode;
        }
        catch (JWTException $e)
        {
            $this->errorCode = "407";
            return $this->errorCode;
        }
    }


}
