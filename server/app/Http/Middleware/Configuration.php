<?php

namespace App\Http\Middleware;

use Closure;

class Configuration
{
    public function getDefaultPaths()
    {
        $DEV_MODE = 0;
        $DEV_PATH = "http://192.168.1.4:8001/";
        $DEFAULT_PATH = "https://vlearn.vivikta.in/";

        if ($DEV_MODE == 1) {
            $DEFAULT_PATH = $DEV_PATH;
        }
        return $DEFAULT_PATH;
    }
    public function getDefaultS3Path()
    {
        $S3_PATH = "https://vivikta.s3-us-west-2.amazonaws.com/";
        return $S3_PATH;
    }
    public function getDefaultProjectPath()
    {
        $PROJECT_PATH = "vlearn";
        return $PROJECT_PATH;
    }
    public function getFileStoragePath()
    {
        $storagePath = 1;
        return $storagePath;
    }
    public function getPaginationcount()
    {
        $paginationCount = 10;
        return $paginationCount;
    }
    public function getSubscriptionRenewalcount()
    {
        $renewalCount = 10;
        return $renewalCount;
    }
}
