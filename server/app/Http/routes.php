<?php
header('Access-Control-Allow-Origin:*');
header('Access-Control-Allow-Methods:GET,POST,PUT,DELETE');

Route::get('/', function () {
    return view('login');
});
Route::get('/dashboard', function () {
    return view('dashboard');
});
Route::get('/clients', function () {
    return view('clients');
});
Route::get('/servers', function () {
    return view('servers');
});
Route::get('/projects', function () {
    return view('projects');
});
Route::get('/viewProjectDetails', function () {
    return view('viewProjectDetails');
});
Route::get('/deployments', function () {
    return view('deployments');
});
Route::get('/deploymentSettings', function () {
    return view('deploymentSettings');
});
Route::get('/viewDeploymentDetails', function () {
    return view('viewDeploymentDetails');
});
Route::get('/viewServerDetails', function () {
    return view('viewServerDetails');
});
Route::group(['prefix' => 'login'],function()
{
    Route::post('authenticate','AuthenticateController@authenticate');

});
Route::group(['prefix' => 'dashboard'],function()
{
    Route::get('getData','DashboardController@getData');

});
Route::group(['prefix' => 'appBuild'],function()
{
    Route::post('add','ShellScriptController@add');
    Route::get('listAll','ShellScriptController@listAll');
    Route::post('updateStatus','ShellScriptController@updateStatus');
    Route::post('test','ShellScriptController@test');

});
Route::group(['prefix' => 'appInfo'],function()
{
    Route::post('add','AppInfoController@add');
    Route::get('listAll','AppInfoController@listAll');
    Route::post('updateStatus','AppInfoController@updateStatus');
});
Route::group(['prefix' => 'server'],function()
{
    Route::post('add','ServersController@add');
    Route::get('listAll','ServersController@listAll');
    Route::get('getDetails','ServersController@getDetails');
    Route::get('getCpuDetails','ServersController@getCpuDetails');
    Route::post('updateCpuDetails','ServersController@updateCpuDetails');

});
Route::group(['prefix' => 'serverEvents'],function()
{
    Route::get('listAll','ServerEventsController@listAll');

});
Route::group(['prefix' => 'database'],function()
{
    Route::post('add','DatabasesController@add');
    Route::get('listAll','DatabasesController@listAll');

});
Route::group(['prefix' => 'vendors'],function()
{
    Route::post('add','VendorsController@add');
    Route::get('listAll','VendorsController@listAll');

});
Route::group(['prefix' => 'deployments'],function()
{
    Route::post('add','DeploymentsController@add');
    Route::get('listAll','DeploymentsController@listAll');
    Route::get('getStatus','DeploymentsController@getStatus');
    Route::post('updateStatus','DeploymentsController@updateStatus');
});
Route::group(['prefix' => 'test'],function()
{
    Route::post('cd','TestController@cd');
    Route::post('test','TestController@test');

});
//------------- AWS APIs --------------//
Route::group(['prefix' => 'aws'],function()
{
    Route::get('getInstanceDetails','AWSController@getInstanceDetails');
    Route::get('getCpuUtilization','AWSController@getCpuUtilization');

    Route::get('getCpuData','TaskScheduleController@getCpuData');
});