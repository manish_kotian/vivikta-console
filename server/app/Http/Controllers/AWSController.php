<?php

namespace App\Http\Controllers;

use Aws\CloudWatch\CloudWatchClient;
use Aws\Ec2\Ec2Client;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class AWSController extends Controller
{
    public function getInstanceDetails($publicIpAddress)
    {
        $awsArray =[];
        $ec2Client = new Ec2Client([
            'region' => 'us-west-2',
            'version' => 'latest',
            'profile' => 'default'
        ]);
        $result = $ec2Client->describeInstances();
        $reservations = $result->get('Reservations');
        for($i=0;$i<count($reservations);$i++)
        {
            $currentReservation = $reservations[$i];
            $instances = $currentReservation['Instances'];
            for($j=0;$j<count($instances);$j++)
            {
                $currentInstance = $instances[$j];

                if($currentInstance['PublicIpAddress'] == $publicIpAddress)
                {
                    $tempArray = [];
                    $tempArray['InstanceId'] = $currentInstance['InstanceId'];
                    $tempArray['InstanceType'] = $currentInstance['InstanceType'];
                    $tempArray['KeyName'] = $currentInstance['KeyName'];
                    $tempArray['KeyName'] = $currentInstance['KeyName'];
                    $tempArray['LaunchTime'] = $currentInstance['LaunchTime'];
                    $tempArray['AvailabilityZone'] = $currentInstance['Placement']['AvailabilityZone'];
                    $tempArray['PublicIpAddress'] = $currentInstance['PublicIpAddress'];
                    $tempArray['PublicDnsName'] = $currentInstance['PublicDnsName'];
                    array_push($awsArray,$tempArray);
                }
            }
        }

        return $awsArray;
    }
    public function getInstanceId($publicIpAddress)
    {
        $ec2Client = new Ec2Client([
            'region' => 'us-west-2',
            'version' => 'latest',
            'profile' => 'default'
        ]);
        $result = $ec2Client->describeInstances();
        $reservations = $result->get('Reservations');
        for($i=0;$i<count($reservations);$i++)
        {
            $currentReservation = $reservations[$i];
            $instances = $currentReservation['Instances'];
            for($j=0;$j<count($instances);$j++)
            {
                $currentInstance = $instances[$j];

                if($currentInstance['PublicIpAddress'] == $publicIpAddress)
                {
                    return $currentInstance['InstanceId'];
                }
            }
        }
    }

    public function getCpuUtilization(Request $request)
    {
        $cpuUtilizationDetailsArray = [];

        $currentTime = Carbon::now();
        $currentTimeDiff = $currentTime->subHour(1);

        $client = CloudWatchClient::factory(array('region' => 'us-west-2',
            'version' => 'latest',
            'profile' => 'default'));
        $cpu = array(
            array('Name' => 'InstanceId', 'Value' => $request->input('instanceId')), // [2]
        );
        $cpu1 = $client->getMetricStatistics(array(
            'Namespace' => 'AWS/EC2',
            'MetricName' => 'CPUUtilization', // [3]
            'Dimensions' => $cpu,
            'StartTime' => strtotime('-1 day'),
            'EndTime' => strtotime('now'),
            'Period' => 300,
            'Statistics' => array('Maximum', 'Minimum'),
        ));

        $dataPoints = $cpu1->get('Datapoints');
        for($i=0;$i<count($dataPoints);$i++)
        {
            $timeStamp =  $dataPoints[$i]['Timestamp'];
            $filterTimeStamp = date_create($timeStamp);
            $filterFormatTimeStamp = $filterTimeStamp->format('Y,m,d');
            $tempArray = [];
            $tempArray['timestamp'] = $filterFormatTimeStamp;
            $tempArray['timestampStr'] = strtotime($dataPoints[$i]['Timestamp']);
            $tempArray['minimum'] = $dataPoints[$i]['Minimum'];
            $tempArray['maximum'] = $dataPoints[$i]['Maximum'];
            array_push($cpuUtilizationDetailsArray,$tempArray);
        }
        /*$currentPage = LengthAwarePaginator::resolveCurrentPage();
        $itemCollection = collect($cpuUtilizationDetailsArray);
        $perPage = 1;
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        $paginatedItems->setPath($request->url());*/
        $this->sort_array_of_array($cpuUtilizationDetailsArray,'timestampStr');
        return $cpuUtilizationDetailsArray;
    }

    public function sort_array_of_array(&$array, $subfield)
    {
        $sortarray = array();
        foreach ($array as $key => $row)
        {
            $sortarray[$key] = abs($row[$subfield]);
        }

        array_multisort($sortarray, SORT_ASC, $array);
    }
}
