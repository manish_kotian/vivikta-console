<?php

namespace App\Http\Controllers;

use App\AppInfo;
use App\Databases;
use App\Http\Middleware\AuthUser;
use App\Http\Middleware\Configuration;
use App\Http\Middleware\GenerateUUID;
use App\Servers;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class AppInfoController extends Controller
{
    public function add(Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $flagValidateInputs = $this->validateAppInfo($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("4001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $generateUniqueId = new GenerateUUID();
                $appId = $generateUniqueId->getUniqueId();

                $addApp = new AppInfo();
                $addApp->appId = $appId;
                $addApp->appName = $request->input('appName');
                $addApp->appDescription = $request->input('appDescription');
                $addApp->databaseId = $request->input('databaseId');
                $addApp->databaseUsername = $request->input('databaseUsername');
                $addApp->databasePassword = $request->input('databasePassword');
                $addApp->gitlabLink = $request->input('gitlabLink');
                $addApp->gitlabUsername = $request->input('gitlabUsername');
                $addApp->appStatus = 0;
                $addApp->save();

                if(!$addApp->save())
                {
                    $returnValues = new ReturnController("4002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("4000","SUCCESS","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }
    protected function validateAppInfo(Request $request)
    {
        $rules = array(
            'appName' => 'required',
            'gitlabLink' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }

    public function listAll(Request $request)
    {
        $getDefaultVariables = new Configuration();
        $paginationCount = $getDefaultVariables->getPaginationcount();

        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $filterType = $request->input('filterType');

            switch ($filterType)
            {
                case "0":
                    $listApp = AppInfo::get();

                    if(count($listApp) <=0)
                    {
                        $returnValues = new ReturnController("5002","FAILURE","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {
                        if(count($listApp) <=$paginationCount)
                        {
                            $listAppFound = [];
                            $k=0;

                            foreach ($listApp as $app)
                            {
                                $tempArray = [];
                                $tempArray['appId'] = $app['appId'];
                                $tempArray['appName']  = $app['appName'];
                                $tempArray['appDescription'] = $app['appDescription'];

                                $tempArray['databaseId'] = $app['databaseId'];
                                $getDatabaseName = Databases::where('databaseId',$app['databaseId'])->first();
                                $tempArray['databaseName'] = $getDatabaseName['databaseName'];

                                $tempArray['databaseUsername'] = $app['databaseUsername'];
                                $tempArray['databasePassword'] = $app['databasePassword'];
                                $tempArray['gitlabLink'] = $app['gitlabLink'];
                                $tempArray['gitlabUsername'] = $app['gitlabUsername'];
                                $tempArray['appStatus'] = $app['appStatus'];
                                $listAppFound[$k] = $tempArray;
                                $k++;
                            }
                            $data = [
                                "lastPage" => "NULL",
                                "data" => $listAppFound];

                            $returnValues = new ReturnController("5000","SUCCESS",$data);
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $listAppFound = [];
                            $k=0;

                            foreach ($listApp as $app)
                            {
                                $tempArray = [];
                                $tempArray['appId'] = $app['appId'];
                                $tempArray['appName']  = $app['appName'];
                                $tempArray['appDescription'] = $app['appDescription'];

                                $tempArray['databaseId'] = $app['databaseId'];
                                $getDatabaseName = Databases::where('databaseId',$app['databaseId'])->first();
                                $tempArray['databaseName'] = $getDatabaseName['databaseName'];

                                $tempArray['databaseUsername'] = $app['databaseUsername'];
                                $tempArray['databasePassword'] = $app['databasePassword'];
                                $tempArray['gitlabLink'] = $app['gitlabLink'];
                                $tempArray['gitlabUsername'] = $app['gitlabUsername'];
                                $tempArray['appStatus'] = $app['appStatus'];
                                $listAppFound[$k] = $tempArray;
                                $k++;
                            }
                            $data=[
                                "total" => $listApp->total(),
                                "nextPageUrl" => $listApp->nextPageUrl(),
                                "previousPageUrl" => $listApp->previousPageUrl(),
                                "currentPage" => $listApp->currentPage(),
                                "lastPage" => $listApp->lastPage(),
                                "data" => $listAppFound
                            ];

                            $returnValues = new ReturnController("5000","SUCCESS",$data);
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    }
                break;
                case "1":
                    $listApp = AppInfo::get();

                    if(count($listApp) <=0)
                    {
                        $returnValues = new ReturnController("5002","FAILURE","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {

                        $listAppFound = [];
                        $k=0;

                        foreach ($listApp as $app)
                        {
                            $tempArray = [];
                            $tempArray['appId'] = $app['appId'];
                            $tempArray['appName']  = $app['appName'];
                            $tempArray['appStatus'] = $app['appStatus'];
                            $listAppFound[$k] = $tempArray;
                            $k++;
                        }

                        $returnValues = new ReturnController("5000","SUCCESS",$listAppFound);
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                break;
                case "2":
                    $listApp = AppInfo::where('appId',$request->input('appId'))->first();

                    if(count($listApp) <=0)
                    {
                        $returnValues = new ReturnController("5002","FAILURE","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {

                        $tempArray = [];
                        $tempArray['appId'] = $listApp['appId'];
                        $tempArray['appName']  = $listApp['appName'];
                        $tempArray['appDescription'] = $listApp['appDescription'];
                        $tempArray['gitlabLink'] = $listApp['gitlabLink'];
                        $tempArray['gitlabUsername'] = $listApp['gitlabUsername'];
                        $tempArray['appStatus'] = $listApp['appStatus'];

                        $returnValues = new ReturnController("5000","SUCCESS",$tempArray);
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    break;
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }

    public function updateStatus(Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $flagValidateInputs = $this->validateUpdateAppStatus($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("11001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {

            }
        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }
    protected function validateUpdateAppStatus(Request $request)
    {
        $rules = array(
            'appId' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }
}
