<?php

namespace App\Http\Controllers;

use App\CpuData;
use App\Http\Middleware\GenerateUUID;
use App\ServerEvents;
use App\ServerMemory;
use App\Servers;
use Carbon\Carbon;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Request;

use Aws\CloudWatch\CloudWatchClient;
use Aws\Ec2\Ec2Client;
use Illuminate\Pagination\LengthAwarePaginator;

class TaskScheduleController extends Controller
{
    public function getCpuData()
    {
        $listServer = Servers::get();

        foreach ($listServer as $server)
        {

            $listMemoryDetails = shell_exec("python pythonFiles/memory.py");
            $listDiskUsage = shell_exec("python pythonFiles/diskUsage.py");

            $listCpu = CpuData::where('serverId',$server['serverId'])
                ->where('serverType', $server['hostedWith'])
                ->where('cpuTimestampInt',strtotime(Carbon::today()))
                ->first();

            if(count($listCpu) <=0)
            {
                $generateUniqueId = new GenerateUUID();
                $logId = $generateUniqueId->getUniqueId();

                $addCpuData = new CpuData();
                $addCpuData->logId = $logId;
                $addCpuData->serverId = $server['serverId'];
                $addCpuData->serverType = $server['hostedWith'];
                $addCpuData->memoryDetails = $listMemoryDetails;
                $addCpuData->diskUsageDetails = $listDiskUsage;
                $addCpuData->cpuTimestampInt = strtotime(Carbon::today());
                $addCpuData->save();

            }
            else
            {

                $updateCpu = CpuData::where('serverId',$server['serverId'])
                    ->where('serverType', $server['hostedWith'])
                    ->where('cpuTimestampInt',strtotime(Carbon::today()))
                    ->update(['memoryDetails',$listMemoryDetails,
                             'diskUsageDetails' => $listDiskUsage]);
            }
        }
    }
}
