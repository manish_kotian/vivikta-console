<?php

namespace App\Http\Controllers;

use App\CpuData;
use App\Credentials;
use App\Http\Middleware\GenerateUUID;
use App\Servers;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class TestController extends Controller
{
    public function cd(Request $request)
    {
        /*$path = public_path();
        //chdir($path);
        $output1  = shell_exec("sh dissite_command.sh");
        echo "dissite output: ".$output1;*/

        $path = public_path();
        chdir($path);
        $process = new Process('sh dissite_command.sh');
        $process->run();

        if(!$process->isSuccessful()){
            throw new ProcessFailedException($process);
        }

        echo $process->getOutput();
    }
    public function test(Request $request)
    {

        $listServer = Servers::get();

        foreach ($listServer as $server)
        {

            $listMemoryDetails = shell_exec("python pythonFiles/memory.py");
            $listDiskUsage = shell_exec("python pythonFiles/diskUsage.py");

            $listCpu = CpuData::where('serverId',$server['serverId'])
                ->where('serverType', $server['hostedWith'])
                ->where('cpuTimestampInt',strtotime(Carbon::today()))
                ->first();

            if(count($listCpu) <=0)
            {
                $generateUniqueId = new GenerateUUID();
                $logId = $generateUniqueId->getUniqueId();

                $addCpuData = new CpuData();
                $addCpuData->logId = $logId;
                $addCpuData->serverId = $server['serverId'];
                $addCpuData->serverType = $server['hostedWith'];
                $addCpuData->memoryDetails = $listMemoryDetails;
                $addCpuData->diskUsageDetails = $listDiskUsage;
                $addCpuData->cpuTimestampInt = strtotime(Carbon::today());
                $addCpuData->save();

            }
            else
            {

                $updateCpu = CpuData::where('serverId',$server['serverId'])
                    ->where('serverType', $server['hostedWith'])
                    ->where('cpuTimestampInt',strtotime(Carbon::today()))
                    ->update(['memoryDetails' => $listMemoryDetails,
                        'diskUsageDetails' => $listDiskUsage]);
            }
        }

    }
    public function listAll(Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $getLoginId = Login::where('remember_token',$request->input('token'))->first();
        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }


}
