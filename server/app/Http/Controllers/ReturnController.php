<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ReturnController extends Controller
{
    protected $statusCode = "0";
    protected $errorCode;
    protected $statusText;
    protected $data;

    public function __construct($errorCode,$statusText,$data)
    {
        $this->errorCode = $errorCode;
        $this->statusText = $statusText;
        $this->data = $data;
    }

    //function to return the values
    public function returnValues()
    {
        if($this->data == null || $this->data == "")
        {
            return response()->json(array("statusCode" => $this->statusCode,
                "errorCode" => $this->errorCode,
                "statusText" => $this->statusText));
        }
        else
        {
            return response()->json(array("statusCode" => $this->statusCode,
                "errorCode" => $this->errorCode,
                "statusText" => $this->statusText,
                "data" => $this->data));
        }
    }
}
