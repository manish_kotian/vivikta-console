<?php

namespace App\Http\Controllers;

use App\CpuData;
use App\Credentials;
use App\Http\Middleware\AuthUser;
use App\Http\Middleware\Configuration;
use App\Http\Middleware\GenerateUUID;
use App\Login;
use App\Servers;
use Aws\CloudWatch\CloudWatchClient;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class ServersController extends Controller
{
    public function add(Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $flagValidateInputs = $this->validateAddServerRequest($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("7001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $generateUniqueId = new GenerateUUID();
                $serverId = $generateUniqueId->getUniqueId();
                $password = '';

                if($request->input('serverPasswordType') == "0")
                {
                    $path = public_path();
                    if(Input::file('serverPasswordPemFile') != "" || Input::file('serverPasswordPemFile') != null)
                    {
                        $fileExtension = pathinfo(Input::file('serverPasswordPemFile')->getClientOriginalName(),PATHINFO_EXTENSION);
                        Input::file('serverPasswordPemFile')->move("$path/serverFiles/", $serverId.".".$fileExtension);
                        $password = "$path/serverFiles/".$serverId.".".$fileExtension;
                    }
                }
                else
                {
                    $password = $request->input('serverPassword');
                }

                $addServer = new Servers();
                $addServer->serverId = $serverId;
                $addServer->serverName = $request->input('serverName');
                $addServer->serverDescription = $request->input('serverDescription');
                $addServer->serverPublicIp = $request->input('serverPublicIp');
                $addServer->hostedWith = $request->input('hostedWith');
                $addServer->serverUsername = $request->input('serverUsername');
                $addServer->serverPasswordType = $request->input('serverPasswordType');
                $addServer->serverPassword = $password;
                $addServer->save();

                if(!$addServer->save())
                {
                    $returnValues = new ReturnController("7002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $credentialId = $generateUniqueId->getUniqueId();
                    if($request->input('hostedWith') == 0){
                        //digital ocean
                        $addCredentials = new Credentials();
                        $addCredentials->credentialId = $credentialId;
                        $addCredentials->serverId = $serverId;
                        $addCredentials->digitalOceanToken = $request->input('digitalOceanToken');
                        $addCredentials->awsAccessKeyId = "N/A";
                        $addCredentials->awsSecretAccessKey = "N/A";
                        $addCredentials->awsRegion = "N/A";
                        $addCredentials->awsProfileName = "N/A";
                        $addCredentials->save();

                        if(!$addCredentials->save())
                        {
                            $returnValues = new ReturnController("7003","FAILURE","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $returnValues = new ReturnController("7000","SUCCESS","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    }
                    else if($request->input('hostedWith') == 1){
                        //aws
                        $addCredentials = new Credentials();
                        $addCredentials->credentialId = $credentialId;
                        $addCredentials->serverId = $serverId;
                        $addCredentials->digitalOceanToken = "N/A";
                        $addCredentials->awsAccessKeyId = $request->input('awsAccessKeyId');
                        $addCredentials->awsSecretAccessKey = $request->input('awsSecretAccessKey');
                        $addCredentials->awsRegion = $request->input('awsRegion');
                        $addCredentials->awsProfileName = $request->input('awsProfileName');
                        $addCredentials->save();

                        if(!$addCredentials->save())
                        {
                            $returnValues = new ReturnController("7003","FAILURE","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $returnValues = new ReturnController("7000","SUCCESS","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    }
                    else{
                        //others
                        $addCredentials = new Credentials();
                        $addCredentials->credentialId = $credentialId;
                        $addCredentials->serverId = $serverId;
                        $addCredentials->digitalOceanToken = "N/A";
                        $addCredentials->awsAccessKeyId = "N/A";
                        $addCredentials->awsSecretAccessKey = "N/A";
                        $addCredentials->awsRegion = "N/A";
                        $addCredentials->awsProfileName = "N/A";
                        $addCredentials->save();

                        if(!$addCredentials->save())
                        {
                            $returnValues = new ReturnController("7003","FAILURE","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $returnValues = new ReturnController("7000","SUCCESS","");
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    }
                }
            }
        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }

    protected function validateAddServerRequest(Request $request)
    {
        $rules = array(
            'serverName' => 'required',
            'serverPublicIp' => 'required',
            'hostedWith' => 'required',
            'serverUsername' => 'required',
            'serverPasswordType' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }

    public function listAll(Request $request)
    {
        $getDefaultVariables = new Configuration();
        $paginationCount = $getDefaultVariables->getPaginationcount();

        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $filterType = $request->input('filterType');

            switch ($filterType)
            {
                case "0":
                    $listServers = Servers::get();

                    if(count($listServers) <=0)
                    {
                        $returnValues = new ReturnController("8002","FAILURE","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {
                        if(count($listServers) <=$paginationCount)
                        {
                            $listServersFound = [];
                            $k=0;

                            foreach ($listServers as $server)
                            {
                                $tempArray = [];
                                $tempArray['serverId'] = $server['serverId'];
                                $tempArray['serverName'] = $server['serverName'];
                                $tempArray['serverDescription'] = $server['serverDescription'];
                                $tempArray['serverPublicIp'] = $server['serverPublicIp'];
                                $tempArray['hostedWith'] = $server['hostedWith'];
                                $tempArray['serverUsername'] = $server['serverUsername'];
                                $tempArray['serverPasswordType'] = $server['serverPasswordType'];
                                $tempArray['serverPassword'] = $server['serverPassword'];
                                $listServersFound[$k] = $tempArray;
                                $k++;
                            }
                            $data = [
                                "lastPage" => "NULL",
                                "data" => $listServersFound];

                            $returnValues = new ReturnController("8000","SUCCESS",$data);
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $listServersFound = [];
                            $k=0;

                            foreach ($listServers as $server)
                            {
                                $tempArray = [];

                                $listServersFound[$k] = $tempArray;
                                $tempArray['serverId'] = $server['serverId'];
                                $tempArray['serverName'] = $server['serverName'];
                                $tempArray['serverDescription'] = $server['serverDescription'];
                                $tempArray['serverPublicIp'] = $server['serverPublicIp'];
                                $tempArray['hostedWith'] = $server['hostedWith'];
                                $tempArray['serverUsername'] = $server['serverUsername'];
                                $tempArray['serverPasswordType'] = $server['serverPasswordType'];
                                $tempArray['serverPassword'] = $server['serverPassword'];
                                $k++;
                            }
                            $data=[
                                "total" => $listServers->total(),
                                "nextPageUrl" => $listServers->nextPageUrl(),
                                "previousPageUrl" => $listServers->previousPageUrl(),
                                "currentPage" => $listServers->currentPage(),
                                "lastPage" => $listServers->lastPage(),
                                "data" => $listServersFound
                            ];

                            $returnValues = new ReturnController("8000","SUCCESS",$data);
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    }
                    break;
                case "1":
                    $listServers = Servers::get();

                    if(count($listServers) <=0)
                    {
                        $returnValues = new ReturnController("8002","FAILURE","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {

                        $listServersFound = [];
                        $k=0;

                        foreach ($listServers as $server)
                        {
                            $tempArray = [];
                            $tempArray['serverId'] = $server['serverId'];
                            $tempArray['serverName'] = $server['serverName'];
                            $listServersFound[$k] = $tempArray;
                            $k++;
                        }

                        $returnValues = new ReturnController("8000","SUCCESS",$listServersFound);
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    break;
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }

    public function getDetails(Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $getLoginId = Login::where('remember_token',$request->input('token'))->first();

            $listServer = Servers::where('serverId',$request->input('serverId'))->first();

            if(count($listServer) <=0)
            {
                $returnValues = new ReturnController("17002","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;

            }
            else
            {
                $checkServerType = $listServer['hostedWith'];

                switch ($checkServerType)
                {
                    case "0":
                    break;
                    case "1":
                        $aws = new AWSController();
                        $awsDetails = $aws->getInstanceDetails($listServer['serverPublicIp']);

                        $returnValues = new ReturnController("17000","SUCCESS",$awsDetails);
                        $return = $returnValues->returnValues();
                        return $return;
                    break;
                    case "2":
                    break;

                }
            }
        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }
    public function getCpuDetails(Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $getLoginId = Login::where('remember_token',$request->input('token'))->first();

            $listServer = Servers::where('serverId',$request->input('serverId'))->first();

            if(count($listServer) <=0)
            {
                $returnValues = new ReturnController("18002","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;

            }
            else
            {
                $checkServerType = $listServer['hostedWith'];

                $currentDate = Carbon::today()->toDateString();


                switch ($checkServerType)
                {
                    case "0":
                        break;
                    case "1":
                            $filterType = $request->input('filterType');

                            switch ($filterType)
                            {
                                case "0":
                                    $listCpuData = CpuData::where('cpu_data_table.serverId',$request->input('serverId'))
                                        ->whereDate('cpu_data_table.created_at','=',$currentDate)
                                        ->get();

                                    if(count($listCpuData) <=0)
                                    {
                                        $returnValues = new ReturnController("18003","FAILURE","");
                                        $return = $returnValues->returnValues();
                                        return $return;
                                    }
                                    else
                                    {
                                        $listCpuDataFound = [];

                                        foreach ($listCpuData as $cpuData)
                                        {

                                            $tempArray = [];
                                            $tempArray['cpuDetails'] = json_decode($cpuData['cpuDetails']);
                                            array_push($listCpuDataFound,$tempArray);

                                        }
                                    }
                                    //$this->sort_array_of_array($listCpuDataFound,'timestampStr');
                                    $returnValues = new ReturnController("18000","SUCCESS",$listCpuDataFound);
                                    $return = $returnValues->returnValues();
                                    return $return;
                                break;
                                case "1":
                                    $startDate = $request->input('startDate');
                                    $filterStartDate = date_create($startDate);
                                    $filterFormatStartDate = $filterStartDate->format('Y-m-d');
                                    $filterFormatStrStartDate = strtotime($filterFormatStartDate);
                                    $endDate = $request->input('endDate');
                                    $filterEndDate = date_create($endDate);
                                    $filterFormatEndDate = $filterEndDate->format('Y-m-d');
                                    $filterFormatStrEndDate = strtotime($filterFormatEndDate);
                                    $listCpuData = CpuData::where('serverId',$request->input('serverId'))
                                        ->where('cpuTimestampInt','>=',$filterFormatStrStartDate)
                                        ->where('cpuTimestampInt','<=',$filterFormatStrEndDate)
                                        ->get();

                                    if(count($listCpuData) <=0)
                                    {
                                        $returnValues = new ReturnController("18003","FAILURE","");
                                        $return = $returnValues->returnValues();
                                        return $return;
                                    }
                                    else
                                    {
                                        $listCpuDataFound = [];

                                        foreach ($listCpuData as $cpuData)
                                        {
                                            $tempArray = [];
                                            $tempArray['cpuDetails'] = json_decode($cpuData['cpuDetails']);
                                            array_push($listCpuDataFound,$tempArray);

                                        }
                                    }
                                    //$this->sort_array_of_array($listCpuDataFound,'timestampStr');
                                    $returnValues = new ReturnController("18000","SUCCESS",$listCpuDataFound);
                                    $return = $returnValues->returnValues();
                                    return $return;
                                break;
                            }

                        break;
                    case "2":
                        break;

                }
            }
        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }
    public function sort_array_of_array(&$array, $subfield)
    {
        $sortarray = array();
        foreach ($array as $key => $row)
        {
            $sortarray[$key] = abs($row[$subfield]);
        }

        array_multisort($sortarray, SORT_ASC, $array);
    }

    public function updateCpuDetails(Request $request)
    {
        $listCpu = CpuData::where('serverId',$request->input('serverId'))
                            ->where('serverType',$request->input('hostedWith'))
                            ->where('cpuTimestampInt',strtotime(Carbon::today()))
                            ->first();

        $cpuDetailsFound = [];
        $memoryDetailsFound = [];
        $diskUsageDetailsFound = [];

        $filterType = $request->input('filterType');

        switch ($filterType)
        {
            case "0":
                if(count($listCpu) <=0)
                {
                    $generateUniqueId = new GenerateUUID();
                    $logId = $generateUniqueId->getUniqueId();

                    $tempCpuArray = [];
                    $tempCpuArray['cpuPercentage'] = $request->input('cpuPercentage');
                    $tempCpuArray['cpuTime'] = Carbon::now()->format('Y-m-d H:i:s');
                    $tempCpuArray['cpuTimeInt'] = strtotime(Carbon::now()->format('Y-m-d H:i:s'));
                    array_push($cpuDetailsFound,$tempCpuArray);

                    $addCpuData = new CpuData();
                    $addCpuData->logId = $logId;
                    $addCpuData->serverId = $request->input('serverId');
                    $addCpuData->serverType = $request->input('hostedWith');
                    $addCpuData->cpuDetails = json_encode($cpuDetailsFound);
                    $addCpuData->cpuTimestampInt = strtotime(Carbon::today());
                    $addCpuData->save();
                }
                else
                {
                    $tempCpuArray = [];
                    $tempCpuArray['cpuPercentage'] = $request->input('cpuPercentage');
                    $tempCpuArray['cpuTime'] = Carbon::now()->format('Y-m-d H:i:s');
                    $tempCpuArray['cpuTimeInt'] = strtotime(Carbon::now()->format('Y-m-d H:i:s'));
                    array_push($cpuDetailsFound,$tempCpuArray);

                    $listOldDetails = $listCpu['cpuDetails'];
                    $fullDetails = $cpuDetailsFound;
                    if($listOldDetails != null)
                    {
                        $listNewDetails = json_encode($cpuDetailsFound);
                        $fullDetails = array_merge(json_decode($listOldDetails,true),json_decode($listNewDetails,true));
                    }

                    $updateCpu = CpuData::where('serverId',$request->input('serverId'))
                        ->where('serverType',$request->input('hostedWith'))
                        ->where('cpuTimestampInt',strtotime(Carbon::today()))
                        ->update(['cpuDetails' => json_encode($fullDetails)]);
                }
                break;
            case "1":
                if(count($listCpu) <=0)
                {
                    $generateUniqueId = new GenerateUUID();
                    $logId = $generateUniqueId->getUniqueId();

                    $tempMemoryArray['availableMemory'] = $request->input('available');
                    $tempMemoryArray['usedMemory'] = $request->input('used');
                    $tempMemoryArray['percentMemory'] = $request->input('percent');
                    $tempMemoryArray['freeMemory'] = $request->input('free');
                    $tempMemoryArray['inactiveMemory'] = $request->input('inactive');
                    $tempMemoryArray['activeMemory'] = $request->input('active');
                    $tempMemoryArray['totalMemory'] = $request->input('total');
                    array_push($memoryDetailsFound,$tempMemoryArray);

                    $addCpuData = new CpuData();
                    $addCpuData->logId = $logId;
                    $addCpuData->serverId = $request->input('serverId');
                    $addCpuData->serverType = $request->input('hostedWith');
                    $addCpuData->memoryDetails = json_encode($memoryDetailsFound);
                    $addCpuData->cpuTimestampInt = strtotime(Carbon::today());
                    $addCpuData->save();
                }
                else
                {
                    $tempMemoryArray = [];
                    $tempMemoryArray['availableMemory'] = $request->input('available');
                    $tempMemoryArray['usedMemory'] = $request->input('used');
                    $tempMemoryArray['percentMemory'] = $request->input('percent');
                    $tempMemoryArray['freeMemory'] = $request->input('free');
                    $tempMemoryArray['inactiveMemory'] = $request->input('inactive');
                    $tempMemoryArray['activeMemory'] = $request->input('active');
                    $tempMemoryArray['totalMemory'] = $request->input('total');
                    array_push($memoryDetailsFound,$tempMemoryArray);

                    $listOldDetails = $listCpu['memoryDetails'];
                    $fullDetails = $memoryDetailsFound;
                    if($listOldDetails != null)
                    {
                        $listNewDetails = json_encode($memoryDetailsFound);
                        $fullDetails = array_merge(json_decode($listOldDetails,true),json_decode($listNewDetails,true));
                    }

                    $updateCpu = CpuData::where('serverId',$request->input('serverId'))
                        ->where('serverType',$request->input('hostedWith'))
                        ->where('cpuTimestampInt',strtotime(Carbon::today()))
                        ->update(['memoryDetails' => json_encode($fullDetails)]);
                }
                break;
            case "2":
                if(count($listCpu) <=0)
                {
                    $generateUniqueId = new GenerateUUID();
                    $logId = $generateUniqueId->getUniqueId();

                    $tempDiskArray = [];
                    $tempDiskArray ['totalDisk'] = $request->input('total');
                    $tempDiskArray ['percentDisk'] = $request->input('percent');
                    $tempDiskArray ['freeDisk'] = $request->input('free');
                    $tempDiskArray ['usedDisk'] = $request->input('used');
                    array_push($diskUsageDetailsFound,$tempDiskArray);

                    $addCpuData = new CpuData();
                    $addCpuData->logId = $logId;
                    $addCpuData->serverId = $request->input('serverId');
                    $addCpuData->serverType = $request->input('hostedWith');
                    $addCpuData->diskUsageDetails = json_encode($diskUsageDetailsFound);
                    $addCpuData->cpuTimestampInt = strtotime(Carbon::today());
                    $addCpuData->save();
                }
                else
                {
                    $tempDiskArray = [];
                    $tempDiskArray ['totalDisk'] = $request->input('total');
                    $tempDiskArray ['percentDisk'] = $request->input('percent');
                    $tempDiskArray ['freeDisk'] = $request->input('free');
                    $tempDiskArray ['usedDisk'] = $request->input('used');
                    array_push($diskUsageDetailsFound,$tempDiskArray);

                    $listOldDetails = $listCpu['diskUsageDetails'];
                    $fullDetails =  $diskUsageDetailsFound;
                    if($listOldDetails != null)
                    {
                        $listNewDetails = json_encode($diskUsageDetailsFound);
                        $fullDetails = array_merge(json_decode($listOldDetails,true),json_decode($listNewDetails,true));
                    }

                    $updateCpu = CpuData::where('serverId',$request->input('serverId'))
                        ->where('serverType',$request->input('hostedWith'))
                        ->where('cpuTimestampInt',strtotime(Carbon::today()))
                        ->update(['diskUsageDetails' => json_encode($fullDetails)]);
                }
                break;
        }

    }
}
