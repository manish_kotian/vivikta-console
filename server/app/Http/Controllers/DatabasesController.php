<?php

namespace App\Http\Controllers;

use App\Databases;
use App\Http\Middleware\AuthUser;
use App\Http\Middleware\GenerateUUID;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class DatabasesController extends Controller
{
    public function add(Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $flagValidateInputs = $this->validateAddDatabaseRequest($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("9001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $generateUniqueId = new GenerateUUID();
                $databaseId = $generateUniqueId->getUniqueId();

                $addDatabase = new Databases();
                $addDatabase->databaseId = $databaseId;
                $addDatabase->databaseName = $request->input('databaseName');
                $addDatabase->save();

                if(!$addDatabase->save())
                {
                    $returnValues = new ReturnController("9002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("9000","SUCCESS","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
            }
        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }

    protected function validateAddDatabaseRequest(Request $request)
    {
        $rules = array(
            'databaseName' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }

    public function listAll(Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $filterType = $request->input('filterType');

            switch ($filterType)
            {
                case "0":
                    $listDatabases = Databases::get();

                    if(count($listDatabases) <=0)
                    {
                        $returnValues = new ReturnController("10002","FAILURE","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {

                        $listDatabasesFound = [];
                        $k=0;

                        foreach ($listDatabases as $database)
                        {
                            $tempArray = [];
                            $tempArray['databaseId'] = $database['databaseId'];
                            $tempArray['databaseName'] = $database['databaseName'];
                            $listDatabasesFound[$k] = $tempArray;
                            $k++;
                        }

                        $returnValues = new ReturnController("10000","SUCCESS",$listDatabasesFound);
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    break;

            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }
}
