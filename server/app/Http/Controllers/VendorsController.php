<?php

namespace App\Http\Controllers;

use App\Http\Middleware\AuthUser;
use App\Http\Middleware\Configuration;
use App\Http\Middleware\GenerateUUID;
use App\Vendors;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class VendorsController extends Controller
{
    public function add(Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $flagValidateInputs = $this->validateAddVendorRequest($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("13001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $generateUniqueId = new GenerateUUID();
                $vendorId = $generateUniqueId->getUniqueId();

                $addVendor = new Vendors();
                $addVendor->vendorId = $vendorId;
                $addVendor->vendorName = $request->input('vendorName');
                $addVendor->save();

                if(!$addVendor->save())
                {
                    $returnValues = new ReturnController("13002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("13000","SUCCESS","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
            }
        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }

    protected function validateAddVendorRequest(Request $request)
    {
        $rules = array(
            'vendorName' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }

    public function listAll(Request $request)
    {
        $getDefaultVariables = new Configuration();
        $paginationCount = $getDefaultVariables->getPaginationcount();

        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $filterType = $request->input('filterType');

            switch ($filterType)
            {
                case "0":
                    $listVendors = Vendors::get();

                    if(count($listVendors) <=0)
                    {
                        $returnValues = new ReturnController("12002","FAILURE","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {
                        if(count($listVendors) <=$paginationCount)
                        {
                            $listVendorsFound = [];
                            $k=0;

                            foreach ($listVendors as $vendor)
                            {
                                $tempArray = [];
                                $tempArray['vendorId'] = $vendor['vendorId'];
                                $tempArray['vendorName'] = $vendor['vendorName'];
                                $listVendorsFound[$k] = $tempArray;
                                $k++;
                            }
                            $data = [
                                "lastPage" => "NULL",
                                "data" => $listVendorsFound];

                            $returnValues = new ReturnController("12000","SUCCESS",$data);
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $listVendorsFound = [];
                            $k=0;

                            foreach ($listVendors as $vendor)
                            {
                                $tempArray = [];
                                $tempArray['vendorId'] = $vendor['vendorId'];
                                $tempArray['vendorName'] = $vendor['vendorName'];
                                $listVendorsFound[$k] = $tempArray;
                                $k++;
                            }
                            $data=[
                                "total" => $listVendors->total(),
                                "nextPageUrl" => $listVendors->nextPageUrl(),
                                "previousPageUrl" => $listVendors->previousPageUrl(),
                                "currentPage" => $listVendors->currentPage(),
                                "lastPage" => $listVendors->lastPage(),
                                "data" => $listVendorsFound
                            ];

                            $returnValues = new ReturnController("12000","SUCCESS",$data);
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    }
                    break;
                case "1":
                    $listVendors = Vendors::get();

                    if(count($listVendors) <=0)
                    {
                        $returnValues = new ReturnController("12002","FAILURE","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {

                        $listVendorsFound = [];
                        $k=0;

                        foreach ($listVendors as $vendor)
                        {
                            $tempArray = [];
                            $tempArray['vendorId'] = $vendor['vendorId'];
                            $tempArray['vendorName'] = $vendor['vendorName'];
                            $listVendorsFound[$k] = $tempArray;
                            $k++;
                        }

                        $returnValues = new ReturnController("12000","SUCCESS",$listVendorsFound);
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    break;
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }
}
