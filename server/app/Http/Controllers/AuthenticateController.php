<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthenticateController extends Controller
{
    public function index()
    {
        // TODO: show users
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('emailId', 'password');

        try {
            // verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        $token = compact('token');
        $data = [
            "token"=>$token];


        $returnValues = new ReturnController("1000", "SUCCESS", $data);
        $return = $returnValues->returnValues();
        return $return->withCookie(cookie('console','true',60,"","",false,true));

    }
}
