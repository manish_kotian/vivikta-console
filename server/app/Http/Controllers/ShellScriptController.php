<?php

namespace App\Http\Controllers;

use App\AppBuild;
use App\AppInfo;
use App\Http\Middleware\AuthUser;
use App\Http\Middleware\Configuration;
use App\Http\Middleware\GenerateUUID;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class ShellScriptController extends Controller
{
    public function add(Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $flagValidateInputs = $this->validateAppBuild($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("2001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $generateUniqueId = new GenerateUUID();
                $buildId = $generateUniqueId->getUniqueId();

                $versionNumber = $request->input('versionNumber');
                $incrementType = $request->input('incrementType');

                $listBuild = AppBuild::where('appId',$request->input('appId'))
                    ->where('buildStatus',1)
                    ->orderby('id','desc')
                    ->first();

                $checkBuild = AppBuild::where('buildStatus',0)->first();

                if(count($checkBuild) > 0)
                {
                    $returnValues = new ReturnController("2003","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }

                $oldVersionNumber = $listBuild['versionNumber'];

                if($oldVersionNumber == null)
                {
                    $oldVersionNumber = 0.1;
                }

                if($incrementType == 0)
                {

                     $versionNumber = $oldVersionNumber + 0.1;

                }

                if($versionNumber <= $oldVersionNumber)
                {
                    $returnValues = new ReturnController("2002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }

                $listAppName = AppInfo::where('appId',$request->input('appId'))->first();

                $appName = $listAppName['appName'];
                $path = public_path();
                $apkPath = "$path/apk";
                $logPath = "$path/log";
                $programPath = "/var/www/html/ionicProjects/$appName";
                $gitPullPath = "$path/gitlab";

                $gitlabLink = $request->input('gitlabLink');
                $gitlabUsername = $request->input('gitlabUsername');
                $gitlabPassword = $request->input('gitlabPassword');

                $gitlabStr = preg_replace('#^https?://#', '', $gitlabLink);
                $gitlabFinalLink = "https://$gitlabUsername:$gitlabPassword@$gitlabStr";

                $output = shell_exec("python script_launch.py $oldVersionNumber $versionNumber $gitlabFinalLink $apkPath $appName $programPath $gitPullPath $buildId $logPath  > /dev/null 2>/dev/null &");
                //file_put_contents("$path/test.txt",$output);

                $appBuild = new AppBuild();
                $appBuild->buildId = $buildId;
                $appBuild->appId = $request->input('appId');
                $appBuild->incrementType = $request->input('incrementType');
                $appBuild->versionNumber = $versionNumber;
                $appBuild->gitlabLink = $request->input('gitlabLink');
                $appBuild->gitlabUsername = $request->input('gitlabUsername');
                $appBuild->gitlabPassword = $request->input('gitlabPassword');
                $appBuild->apkPath = "N/A";
                $appBuild->logFilePath = "N/A";
                $appBuild->buildStatus = 0;
                $appBuild->save();

                if(!$appBuild->save())
                {
                    $returnValues = new ReturnController("2003","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("2000","SUCCESS","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }

    protected function validateAppBuild(Request $request)
    {
        $rules = array(
            'appId' => 'required',
            'gitlabLink' => 'required',
            'gitlabUsername' => 'required',
            'gitlabPassword' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }

    public function listAll(Request $request)
    {
        $getDefaultVariables = new Configuration();
        $paginationCount = $getDefaultVariables->getPaginationcount();

        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $listBuild = AppBuild::where('appId',$request->input('appId'))
                ->orderby('id','desc')
                ->get();

            if(count($listBuild) <=0)
            {
                $returnValues = new ReturnController("3002","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                if(count($listBuild) <=$paginationCount)
                {
                    $listBuildFound = [];
                    $k=0;

                    foreach ($listBuild as $build)
                    {
                        $listAppName = AppInfo::where('appId',$build['appId'])->first();

                        $tempArray = [];
                        $tempArray['buildId'] = $build['buildId'];
                        $tempArray['appName'] = $listAppName['appName'];
                        $tempArray['incrementType'] = $build['incrementType'];
                        $tempArray['versionNumber'] = $build['versionNumber'];
                        $tempArray['gitlabLink'] = $build['gitlabLink'];
                        $tempArray['gitlabUsername'] = $build['gitlabUsername'];
                        $tempArray['gitlabPassword'] = $build['gitlabPassword'];
                        $tempArray['apkPath'] = $build['apkPath'];
                        $tempArray['logFilePath'] = $build['logFilePath'];
                        $tempArray['buildStatus'] = $build['buildStatus'];
                        $listBuildFound [$k] = $tempArray;
                        $k++;

                    }

                    $data = [
                        "lastPage" => "NULL",
                        "data" => $listBuildFound];

                    $returnValues = new ReturnController("3000","SUCCESS",$data);
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $listBuild = AppBuild::where('appId',$request->input('appId'))
                        ->orderby('id','desc')
                        ->paginate($paginationCount);
                    $listBuildFound = [];
                    $k=0;

                    foreach ($listBuild as $build)
                    {
                        $tempArray = [];
                        $tempArray['buildId'] = $build['buildId'];
                        $tempArray['appName'] = $build['appName'];
                        $tempArray['incrementType'] = $build['incrementType'];
                        $tempArray['versionNumber'] = $build['versionNumber'];
                        $tempArray['gitlabLink'] = $build['gitlabLink'];
                        $tempArray['gitlabUsername'] = $build['gitlabUsername'];
                        $tempArray['gitlabPassword'] = $build['gitlabPassword'];
                        $tempArray['apkPath'] = $build['apkPath'];
                        $tempArray['logFilePath'] = $build['logFilePath'];
                        $tempArray['buildStatus'] = $build['buildStatus'];
                        $listBuildFound [$k] = $tempArray;
                        $k++;

                    }

                    $data=[
                        "total" => $listBuild->total(),
                        "nextPageUrl" => $listBuild->nextPageUrl(),
                        "previousPageUrl" => $listBuild->previousPageUrl(),
                        "currentPage" => $listBuild->currentPage(),
                        "lastPage" => $listBuild->lastPage(),
                        "data" => $listBuildFound
                    ];

                    $returnValues = new ReturnController("3000","SUCCESS",$data);
                    $return = $returnValues->returnValues();
                    return $return;
                }
            }
        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }


    public function updateStatus(Request $request)
    {
        $listApp = AppBuild::where('buildStatus',0)->get();

        if(count($listApp) <=0)
        {
            $returnValues = new ReturnController("4002","FAILURE","");
            $return = $returnValues->returnValues();
            return $return;
        }
        else
        {
            foreach ($listApp as $app)
            {
                $buildId = $app['buildId'];

                $path = public_path();

                $destinationApkPath = $path.'/apk/'.$buildId."."."apk";
                $destinationLogPath =  $path.'/log/'.$buildId."."."txt";

                $checkApkFileExist = file_exists($destinationApkPath);

                if($checkApkFileExist == 1)
                {
                    $listApp = AppInfo::where('appId',$app['appId'])->first();
                    $appName = $listApp['appName'];

                    $APP_S3_Path = "console/$appName/apk/".$buildId."."."apk";
                    $LOG_S3_Path = "console/$appName/log/".$buildId."."."txt";

                    Storage::disk('s3')->put($LOG_S3_Path, file_get_contents($destinationLogPath));
                    Storage::disk('s3')->setVisibility($LOG_S3_Path, 'public');
                    $finalLogUrl = Storage::disk('s3')->url($LOG_S3_Path);

                    Storage::disk('s3')->put($APP_S3_Path, file_get_contents($destinationApkPath));
                    Storage::disk('s3')->setVisibility($APP_S3_Path, 'public');
                    $finalApkUrl = Storage::disk('s3')->url($APP_S3_Path);

                    $updateBuild = AppBuild::where('buildId',$buildId)
                        ->update(['apkPath' => $finalApkUrl,
                            'logFilePath' => $finalLogUrl,
                            'buildStatus' => 1,
                            'buildString' => "N/A"]);

                    unlink($destinationApkPath);
                    unlink($destinationLogPath);

                    //exec("./javakiller.sh");

                }
                else
                {
                    continue;
                }
            }
        }

    }


    public function moveLogFIle(Request $request)
    {
        $listApp = AppBuild::where('buildStatus',0)
            ->where('created_at', '<', Carbon::now()->subMinutes(5)->toDateTimeString())
            ->get();

        if(count($listApp) <=0)
        {
            $returnValues = new ReturnController("4002","FAILURE","");
            $return = $returnValues->returnValues();
            return $return;
        }
        else
        {
            foreach ($listApp as $app)
            {
                $buildId = $app['buildId'];

                $path = public_path();
                $destinationLogPath =  $path.'/log/'.$buildId."."."txt";

                $checkApkFileExist = file_exists($destinationLogPath);

                if($checkApkFileExist == 1)
                {
                    $listApp = AppInfo::where('appId',$app['appId'])->first();
                    $appName = $listApp['appName'];

                    $LOG_S3_Path = "console/$appName/log/".$buildId."."."txt";

                    Storage::disk('s3')->put($LOG_S3_Path, file_get_contents($destinationLogPath));
                    Storage::disk('s3')->setVisibility($LOG_S3_Path, 'public');
                    $finalLogUrl = Storage::disk('s3')->url($LOG_S3_Path);

                    $updateBuild = AppBuild::where('buildId',$buildId)
                        ->update(['logFilePath' => $finalLogUrl,
                            'buildStatus' => 2,
                            'buildString' => "N/A"]);

                    unlink($destinationLogPath);

                   // exec("./javakiller.sh");

                }
                else
                {
                    continue;
                }
            }
        }
    }

    public function test()
    {
       /* $process = new Process('sudo sh javakiller.sh');
        $process->run();

        if(!$process->isSuccessful())
        {
            throw new ProcessFailedException($process);
        }
        echo $process->getOutput();*/

         $output = exec("./javakiller.sh");
         echo $output;
    }
}
