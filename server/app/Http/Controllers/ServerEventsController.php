<?php

namespace App\Http\Controllers;

use App\Http\Middleware\AuthUser;
use App\Http\Middleware\Configuration;
use App\ServerEvents;
use ClassPreloader\Config;
use Illuminate\Http\Request;

use App\Http\Requests;

class ServerEventsController extends Controller
{
    public function listAll(Request $request)
    {
        $getDefaultVariables = new Configuration();
        $paginationCount = $getDefaultVariables->getPaginationcount();

        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $listServerEvents = ServerEvents::where('serverId',$request->input('serverId'))->get();

            if(count($listServerEvents) <=0)
            {
                $returnValues = new ReturnController("19002","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $filterType = $request->input('filterType');
                $listServerEventsFound = [];

                switch ($filterType)
                {
                    case "0":
                        if(count($listServerEvents) <=$paginationCount)
                        {

                            foreach ($listServerEvents as $server)
                            {
                                $timeStamp =  $server['cpuTimestamp'];
                                $filterTimeStamp = date_create($timeStamp);
                                $filterFormatTimeStamp = $filterTimeStamp->format('m/d/Y');

                                $tempArray = [];
                                $tempArray['timestamp'] = $filterFormatTimeStamp;
                                $tempArray['timestampStr'] = strtotime($timeStamp);
                                $tempArray['minimum'] = $server['maximumCpuUsage'];
                                $tempArray['maximum'] = $server['minimumCpuUsage'];
                                array_push($listServerEventsFound,$tempArray);
                            }
                            $data = [
                                "lastPage" => "NULL",
                                "data" => $listServerEventsFound];

                            $returnValues = new ReturnController("8000","SUCCESS",$data);
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $listServerEvents = ServerEvents::where('serverId',$request->input('serverId'))->paginate($paginationCount);

                            foreach ($listServerEvents as $server)
                            {
                                $timeStamp =  $server['cpuTimestamp'];
                                $filterTimeStamp = date_create($timeStamp);
                                $filterFormatTimeStamp = $filterTimeStamp->format('m/d/Y');

                                $tempArray = [];
                                $tempArray['timestamp'] = $filterFormatTimeStamp;
                                $tempArray['timestampStr'] = strtotime($timeStamp);
                                $tempArray['minimum'] = $server['maximumCpuUsage'];
                                $tempArray['maximum'] = $server['minimumCpuUsage'];
                                array_push($listServerEventsFound,$tempArray);
                            }
                            $data=[
                                "total" => $listServerEvents->total(),
                                "nextPageUrl" => $listServerEvents->nextPageUrl(),
                                "previousPageUrl" => $listServerEvents->previousPageUrl(),
                                "currentPage" => $listServerEvents->currentPage(),
                                "lastPage" => $listServerEvents->lastPage(),
                                "data" => $listServerEventsFound
                            ];

                            $returnValues = new ReturnController("8000","SUCCESS",$data);
                            $return = $returnValues->returnValues();
                            return $return;

                        }
                    break;
                    case "1":
                        foreach ($listServerEvents as $server)
                        {
                            $timeStamp =  $server['cpuTimestamp'];
                            $filterTimeStamp = date_create($timeStamp);
                            $filterFormatTimeStamp = $filterTimeStamp->format('m/d/Y');

                            $tempArray = [];
                            $tempArray['timestamp'] = $filterFormatTimeStamp;
                            $tempArray['timestampStr'] = strtotime($timeStamp);
                            $tempArray['minimum'] = $server['maximumCpuUsage'];
                            $tempArray['maximum'] = $server['minimumCpuUsage'];
                            array_push($listServerEventsFound,$tempArray);
                        }

                        $returnValues = new ReturnController("8000","SUCCESS",$listServerEventsFound);
                        $return = $returnValues->returnValues();
                        return $return;
                    break;
                }
                $returnValues = new ReturnController("19000","SUCCESS",$listServerEvents);
                $return = $returnValues->returnValues();
                return $return;
            }
        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }
}
