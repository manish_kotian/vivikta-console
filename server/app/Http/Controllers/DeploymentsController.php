<?php

namespace App\Http\Controllers;

use App\AppInfo;
use App\Deployments;
use App\Http\Middleware\AuthUser;
use App\Http\Middleware\Configuration;
use App\Http\Middleware\GenerateUUID;
use App\Servers;
use App\Vendors;
use Couchbase\Document;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class DeploymentsController extends Controller
{
    public function add(Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $flagValidateInputs = $this->validateAddDeploymentRequest($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("14001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $generateUniqueId = new GenerateUUID();
                $deploymentId = $generateUniqueId->getUniqueId();

                $addDeployment = new Deployments();
                $addDeployment->deploymentId = $deploymentId;
                $addDeployment->deploymentName = $request->input('deploymentName');
                $addDeployment->deploymentDescription = $request->input('deploymentDescription');
                $addDeployment->deploymentDirectoryPath = $request->input('deploymentDirectoryPath');
                $addDeployment->deploymentUrl = $request->input('deploymentUrl');
                $addDeployment->deploymentVirtualHostName = $request->input('deploymentVirtualHostName');
                $addDeployment->deploymentVendorId = $request->input('deploymentVendorId');
                $addDeployment->deploymentHostedOn = $request->input('deploymentHostedOn');
                $addDeployment->deploymentProjectId = $request->input('deploymentProjectId');
                $addDeployment->deploymentStatus = 0;
                $addDeployment->save();

                if(!$addDeployment->save())
                {
                    $returnValues = new ReturnController("14002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $returnValues = new ReturnController("14000","SUCCESS","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
            }
        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }

    protected function validateAddDeploymentRequest(Request $request)
    {
        $rules = array(
            'deploymentName' => 'required',
            'deploymentDirectoryPath' => 'required',
            'deploymentUrl' => 'required',
            'deploymentVirtualHostName' => 'required',
            'deploymentVendorId' => 'required',
            'deploymentHostedOn' => 'required',
            'deploymentProjectId' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }

    public function listAll(Request $request)
    {
        $getDefaultVariables = new Configuration();
        $paginationCount = $getDefaultVariables->getPaginationcount();

        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $filterType = $request->input('filterType');

            switch ($filterType)
            {
                case "0":
                    $listDeployments = Deployments::get();

                    if(count($listDeployments) <=0)
                    {
                        $returnValues = new ReturnController("15002","FAILURE","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {
                        if(count($listDeployments) <=$paginationCount)
                        {
                            $listDeploymentsFound = [];
                            $k=0;

                            foreach ($listDeployments as $deployment)
                            {
                                $tempArray = [];
                                $tempArray['deploymentId'] = $deployment['deploymentId'];
                                $tempArray['deploymentName'] = $deployment['deploymentName'];
                                $tempArray['deploymentDescription'] = $deployment['deploymentDescription'];
                                $tempArray['deploymentUrl'] = $deployment['deploymentUrl'];
                                $tempArray['deploymentVirtualHostName'] = $deployment['deploymentVirtualHostName'];
                                $tempArray['deploymentDirectoryPath'] = $deployment['deploymentDirectoryPath'];

                                $tempArray['deploymentVendorId'] = $deployment['deploymentVendorId'];
                                $getVendorDetails = Vendors::where('vendorId',$deployment['deploymentVendorId'])->first();
                                $tempArray['deploymentVendorName'] = $getVendorDetails['vendorName'];

                                $tempArray['deploymentHostedOn'] = $deployment['deploymentHostedOn'];
                                $getHostedServerDetails = Servers::where('serverId',$deployment['deploymentHostedOn'])->first();
                                $tempArray['deploymentServerName'] = $getHostedServerDetails['serverName'];

                                $tempArray['deploymentProjectId'] = $deployment['deploymentProjectId'];
                                $getProjectDetails = AppInfo::where('appId',$deployment['deploymentProjectId'])->first();
                                $tempArray['deploymentProjectName'] = $getProjectDetails['appName'];

                                $tempArray['deploymentStatus'] = $deployment['deploymentStatus'];
                                $listDeploymentsFound[$k] = $tempArray;
                                $k++;
                            }
                            $data = [
                                "lastPage" => "NULL",
                                "data" => $listDeploymentsFound];

                            $returnValues = new ReturnController("15000","SUCCESS",$data);
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                        else
                        {
                            $listDeploymentsFound = [];
                            $k=0;

                            foreach ($listDeployments as $deployment)
                            {
                                $tempArray = [];
                                $tempArray['deploymentId'] = $deployment['deploymentId'];
                                $tempArray['deploymentName'] = $deployment['deploymentName'];
                                $tempArray['deploymentDescription'] = $deployment['deploymentDescription'];
                                $tempArray['deploymentUrl'] = $deployment['deploymentUrl'];
                                $tempArray['deploymentVirtualHostName'] = $deployment['deploymentVirtualHostName'];
                                $tempArray['deploymentDirectoryPath'] = $deployment['deploymentDirectoryPath'];

                                $tempArray['deploymentVendorId'] = $deployment['deploymentVendorId'];
                                $getVendorDetails = Vendors::where('vendorId',$deployment['deploymentVendorId'])->first();
                                $tempArray['deploymentVendorName'] = $getVendorDetails['vendorName'];
                                
                                $tempArray['deploymentHostedOn'] = $deployment['deploymentHostedOn'];
                                $getHostedServerDetails = Servers::where('serverId',$deployment['deploymentHostedOn'])->first();
                                $tempArray['deploymentServerName'] = $getHostedServerDetails['serverName'];

                                $tempArray['deploymentProjectId'] = $deployment['deploymentProjectId'];
                                $getProjectDetails = AppInfo::where('appId',$deployment['deploymentProjectId'])->first();
                                $tempArray['deploymentProjectName'] = $getProjectDetails['appName'];

                                $tempArray['deploymentStatus'] = $deployment['deploymentStatus'];
                                $listDeploymentsFound[$k] = $tempArray;
                                $k++;
                            }
                            $data=[
                                "total" => $listDeployments->total(),
                                "nextPageUrl" => $listDeployments->nextPageUrl(),
                                "previousPageUrl" => $listDeployments->previousPageUrl(),
                                "currentPage" => $listDeployments->currentPage(),
                                "lastPage" => $listDeployments->lastPage(),
                                "data" => $listDeploymentsFound
                            ];

                            $returnValues = new ReturnController("15000","SUCCESS",$data);
                            $return = $returnValues->returnValues();
                            return $return;
                        }
                    }
                    break;
                case "1":
                    $listDeployments = Deployments::get();

                    if(count($listDeployments) <=0)
                    {
                        $returnValues = new ReturnController("15002","FAILURE","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {

                        $listDeploymentsFound = [];
                        $k=0;

                        foreach ($listDeployments as $deployment)
                        {
                            $tempArray = [];
                            $tempArray['deploymentId'] = $deployment['deploymentId'];
                            $tempArray['deploymentName'] = $deployment['deploymentName'];
                            $tempArray['deploymentStatus'] = $deployment['deploymentStatus'];
                            $listDeploymentsFound[$k] = $tempArray;
                            $k++;
                        }

                        $returnValues = new ReturnController("15000","SUCCESS",$listDeploymentsFound);
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    break;
            }

        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }

    public function getStatus(Request $request)
    {
        $checkDeployment = Deployments::where('deploymentId',Input::get('deploymentId'))->first();

        if(count($checkDeployment) <=0)
        {
            $returnValues = new ReturnController("16002","FAILURE","");
            $return = $returnValues->returnValues();
            return $return;
        }
        else
        {
            $data = [
                "deploymentStatus" => $checkDeployment['deploymentStatus']
            ];
            $returnValues = new ReturnController("16000","SUCCESS",$data);
            $return = $returnValues->returnValues();
            return $return;
        }
    }
    
    public function updateStatus(Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser  = $authenticate->authenticateUser();
        if($authenticateUser == 400)
        {
            $flagValidateInputs = $this->validateUpdateDeploymentStatusRequest($request);
            if($flagValidateInputs == false)
            {
                $returnValues = new ReturnController("17001","FAILURE","");
                $return = $returnValues->returnValues();
                return $return;
            }
            else
            {
                $getDeploymentDetails = Deployments::where('deploymentId',$request->input('deploymentId'))
                                        ->first();
                if(count($getDeploymentDetails) <= 0)
                {
                    $returnValues = new ReturnController("17002","FAILURE","");
                    $return = $returnValues->returnValues();
                    return $return;
                }
                else
                {
                    $updateDeploymentStatus = Deployments::where('deploymentId',$request->input('deploymentId'))
                                                ->update(['deploymentStatus' => $request->input('deploymentStatus')]);

                    if($updateDeploymentStatus < 0)
                    {
                        $returnValues = new ReturnController("17003","SUCCESS","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                    else
                    {
                        $returnValues = new ReturnController("17000","SUCCESS","");
                        $return = $returnValues->returnValues();
                        return $return;
                    }
                }
            }
        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }

    protected function validateUpdateDeploymentStatusRequest(Request $request)
    {
        $rules = array(
            'deploymentId' => 'required',
            'deploymentStatus' => 'required');

        $validator = Validator::make(Input::all(), $rules);
        if($validator->fails())
            return false;
        else
            return true;
    }
}
