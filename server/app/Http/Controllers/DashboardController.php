<?php

namespace App\Http\Controllers;

use App\AppBuild;
use App\AppInfo;
use App\Deployments;
use App\Http\Middleware\AuthUser;
use App\Servers;
use App\Vendors;
use Illuminate\Http\Request;

use App\Http\Requests;

class DashboardController extends Controller
{
    private $totalAppCount;
    private $totalBuildCount;
    private $totalVendorsCount;
    private $totalServersCount;
    private $totalDeploymentsCount;

    public function getData(Request $request)
    {
        $authenticate = new AuthUser();
        $authenticateUser = $authenticate->authenticateUser();
        if($authenticateUser == "400")
        {
            $this->totalVendorsCount = $this->getVendorsCount($request);
            $this->totalServersCount = $this->getServersCount($request);
            $this->totalAppCount = $this->getAppCount($request);
            $this->totalDeploymentsCount = $this->getDeploymentsCount($request);
            $this->totalBuildCount = $this->getBuildCount($request);

            $dashboardData = [
                "totalVendorsCount" => $this->totalVendorsCount,
                "totalServersCount" => $this->totalServersCount,
                "totalAppCount" => $this->totalAppCount,
                "totalDeploymentsCount" => $this->totalDeploymentsCount,
                "totalBuildCount" => $this->totalBuildCount];

            $returnValues = new ReturnController("6000","SUCCESS",$dashboardData);
            $return = $returnValues->returnValues();
            return $return;
        }
        else
        {
            switch($authenticateUser)
            {
                case "404":$returnValues = new ReturnController("404","FAILURE","INVALID_USER");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "405":$returnValues = new ReturnController("405","FAILURE","TOKEN_EXPIRED");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "406":$returnValues = new ReturnController("406","FAILURE","INVALID_TOKEN");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
                case "407":$returnValues = new ReturnController("407","FAILURE","TOKEN_ABSENT");
                    $return = $returnValues->returnValues();
                    return $return;
                    break;
            }
        }
    }

    public function getVendorsCount(Request $request)
    {
        $vendorsCount = Vendors::count();
        return $vendorsCount;
    }

    public function getServersCount(Request $request)
    {
        $serversCount = Servers::count();
        return $serversCount;
    }

    public function getDeploymentsCount(Request $request){
        $deploymentsCount = Deployments::count();
        return $deploymentsCount;
    }

    public function getAppCount(Request $request)
    {
        $appCount = AppInfo::count();
         return $appCount;
    }

    public function getBuildCount(Request $request)
    {
        $buildCount = AppBuild::count();
        return $buildCount;
    }
}
